﻿using BattleNET;
using Nickbot.CommandProvider;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Nickbot.Module.ModuleAttributes(
    "BattleNET",
    "0.1",
    "Nick \"Tiaqo\" Strohm",
    "Greift auf die Battle.net API zu."
    )]
[Nickbot.Module.ModuleHandlerAttributes(
    ""
    )]
public class Main : Nickbot.Module.Module
{
    public override void onLoad()
    {
        AddCommand(new CommandStub("battlenet", "Greift auf die Battle.net API zu.", "", PermissionType.User, 5, e => 
        {
            BattleNETClient.BattleNetRegion region;
            switch (e.Args[0].ToLower())
            {
                case "us":
                    region = BattleNETClient.BattleNetRegion.US;
                    break;
                case "eu":
                    region = BattleNETClient.BattleNetRegion.EU;
                    break;
                case "kr":
                    region = BattleNETClient.BattleNetRegion.KR;
                    break;
                case "tw":
                    region = BattleNETClient.BattleNetRegion.TW;
                    break;
                default:
                    e.Channel.SendMessage($"<@{e.Author.ID}> Diese Region wurde nicht gefunden. Wähle aus US, EU, KR und TW");
                    return;
            }
            BattleNETClient battlenetclient;
            using (StreamReader sr = new StreamReader("credentials.txt"))
            {
                sr.ReadLine();
                sr.ReadLine();
                battlenetclient = new BattleNETClient(sr.ReadLine(), sr.ReadLine(), region, BattleNETClient.BattleNetLocale.de_DE);

                sr.Close();
            }
            
            switch (e.Args[1].ToLower())
            {
                case "wow":
                    switch(e.Args[2].ToLower())
                    {
                        case "achievement":
                            BattleNET.Objects.WOW.Achievement achievement = battlenetclient.WOW.getAchievement(int.Parse(e.Args[3]));
                            string tOutput = $"<@{e.Author.ID}> [{achievement.id}] **{achievement.title}**: {achievement.description}{System.Environment.NewLine}{achievement.reward}";
                            if (achievement.criteria != null && achievement.criteria.Count > 0)
                            {
                                tOutput = $"{tOutput}{System.Environment.NewLine}```{System.Environment.NewLine}";
                                foreach (BattleNET.Objects.WOW.Achievement criteria in achievement.criteria)
                                {
                                    tOutput = $"{tOutput}[{criteria.id}] {criteria.description}{System.Environment.NewLine}";
                                }
                                tOutput = $"{tOutput}```";
                            }
                            e.Channel.SendMessage(tOutput);
                            break;
                    }
                    break;
                case "d3":

                    break;
                case "sc2":
                    break;
            }

        }));
    }

    public override void onUnload()
    {
        RemoveCommand("battlenet");
    }
}
