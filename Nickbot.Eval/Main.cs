﻿using Nickbot;
using Nickbot.Eval;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Threading;
using System.Threading.Tasks;
using Nickbot.CommandProvider;
using System.ComponentModel;

[Nickbot.Module.ModuleAttributes(
    "Eval",
    "0.2",
    "Nick \"Tiaqo\" Strohm",
    "Evaluates C#-Code, that was compiled dynamically."
    )]
[Nickbot.Module.ModuleHandlerAttributes(
    ""
    )]
public class Main : Nickbot.Module.Module
{
    CSharpCodeProvider provider;
    CompilerParameters parameters;

    public override void onLoad()
    {
        provider = new CSharpCodeProvider();
        parameters = new CompilerParameters();

        parameters.ReferencedAssemblies.Add("DSharpPlus.dll");
        parameters.ReferencedAssemblies.Add("Nickbot.dll");
        parameters.ReferencedAssemblies.Add("Nickbot.CommandProvider.dll");
        parameters.GenerateInMemory = true;
        parameters.GenerateExecutable = true;

        string[] EvalNamespaces = new string[]
        {
            "DSharpPlus",
            "DSharpPlus.Objects",
            "Nickbot",
            "Nickbot.CommandProvider",
            "System.Threading",
            "System.Data.Linq",
            "System.Collections.Generic"
        };

        /*AddCommand(new CommandStub("eval", "Evaluates C#-Code in realtime.",
            $"Evaluates C#-Code, that was compiled dynamically.{System.Environment.NewLine}{System.Environment.NewLine}The following assemblies are available:{System.Environment.NewLine} * DSharpPlus{System.Environment.NewLine} * System.Threading{System.Environment.NewLine} * DSharpPlus.Objects{System.Environment.NewLine}{System.Environment.NewLine}{System.Environment.NewLine}Your code should return a string.{System.Environment.NewLine}To access the Discordclient, please use discordClient.",
            PermissionType.Admin, 1, e =>
            {
                string whatToEval = e.Args[0];
                if (whatToEval.StartsWith("`") && whatToEval.EndsWith("`"))
                    whatToEval = whatToEval.Trim('`');
                try
                {
                    var eval = EvalProvider.CreateEvalMethod<Client, string>(whatToEval, EvalNamespaces, new string[] { "DSharpPlus.dll", "Nickbot.dll", "Nickbot.CommandProvider.dll", "System.Data.Linq.dll" });
                    string res = "";
                    Thread.Sleep(1000);
                    Thread executionThread = null;
                    DateTime startTime = DateTime.Now;
                    BackgroundWorker bgWrk = new BackgroundWorker();
                    bgWrk.DoWork += (sender, es) =>
                    {
                        executionThread = Thread.CurrentThread;
                        if (eval != null)
                        {
                            try
                            {
                                res = eval(parent);
                            }
                            catch (Exception ex) { res = "Exception occurred while running: " + ex.Message; }
                        }
                        else
                        {
                            string errors = "Errors While Compiling: \n";
                            if (EvalProvider.errors != null)
                            {
                                if (EvalProvider.errors.Count > 0)
                                {
                                    foreach (var error in EvalProvider.errors)
                                    {
                                        errors += $"{error.ToString()}\n\n";
                                    }
                                }
                                e.Channel.SendMessage($"```\n{errors}\n```");
                            }
                            else
                                e.Channel.SendMessage("Errors!");
                        }
                    };
                    bgWrk.RunWorkerAsync();
                    while (startTime.AddSeconds(10) < DateTime.Now)
                    {
                        if (res == null || res == "")
                            e.Channel.SendMessage("Terminated after 10 second timeout.");
                        else
                            e.Channel.SendMessage($"**Result**\n```\n{res}\n```");
                        break;
                    }
                }
                catch (Exception ex)
                {
                    string errors = "Errors While Compiling: \n";
                    if (EvalProvider.errors != null)
                    {
                        if (EvalProvider.errors.Count > 0)
                        {
                            foreach (var error in EvalProvider.errors)
                            {
                                errors += $"{error.ToString()}\n\n";
                            }
                        }
                        else
                            errors += ex.Message;
                        e.Channel.SendMessage($"```\n{errors}\n```");
                    }
                    else
                        e.Channel.SendMessage($"Errors! {ex.Message}");
                }
                //e.Channel.SendMessage($"This is currently not supported");
            }));*/
    }

    public override void onUnload()
    {
        provider.Dispose();
        //parent.RemoveCommand("eval");
    }
}
