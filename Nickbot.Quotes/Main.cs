﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nickbot;
using Nickbot.CommandProvider;
using Nickbot.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Nickbot.Module.ModuleAttributes(
    "Quotes",
    "0.1",
    "Nick \"Tiaqo\" Strohm",
    ""
    )]
[Nickbot.Module.ModuleHandlerAttributes(
    ""
    )]
public class Main : Nickbot.Module.Module
{
    public class QuoteProvider
    {
        [JsonProperty("serverId")]
        public string serverId { get; internal set; }
        [JsonProperty("quotes")]
        public List<string> quotes { get; internal set; }
    }
    public List<QuoteProvider> dict = new List<QuoteProvider>();

    public override void onLoad()
    {
        JArray arr = JArray.Parse(loadConfig());
        foreach(JObject JQuote in arr)
        {
            dict.Add(new QuoteProvider()
            {
                serverId = JQuote["serverId"].ToString(),
                quotes = new List<string>()
            });
            foreach(string JQuoteSub in JQuote["quotes"])
                dict[dict.Count - 1].quotes.Add(JQuoteSub);
        }

        AddCommand(new CommandStub("quote", "", "", PermissionType.Owner, 2, e =>
        {
            if (dict.Find(x => x.serverId == e.Channel.Parent.ID) == null)
            {
                dict.Add(new QuoteProvider() { serverId = e.Channel.Parent.ID, quotes = new List<string>() });
            } else
            {
                Log(new LogMessage() { Level = MessageLevel.Debug, Message = "Found server in list", TimeStamp = DateTime.Now });
            }

            if (e.Args.Count == 0)
            {
                var random = new Random();
                int rng = random.Next(dict.Find(x => x.serverId == e.Channel.Parent.ID).quotes.Count());
                if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages) && dict.Find(x => x.serverId == e.Channel.Parent.ID).quotes.Count() > 0)
                {
                    e.Channel.SendMessage($"Quotes #{rng}:{System.Environment.NewLine}{dict.Find(x => x.serverId == e.Channel.Parent.ID).quotes[rng]}");
                }
            }
            else if (e.Args.Count == 1)
            {
                if (isNumeric(e.Args[0]))
                {
                    if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages) && dict.Find(x => x.serverId == e.Channel.Parent.ID).quotes.Count() > int.Parse(e.Args[0]))
                        e.Channel.SendMessage($"Quote #{e.Args[0]}:{System.Environment.NewLine}{dict.Find(x => x.serverId == e.Channel.Parent.ID).quotes[int.Parse(e.Args[0])]}");
                }
            }
            else if (e.Args.Count == 2)
            {
                if (e.Args[1] == "add")
                {
                    dict.Find(x => x.serverId == e.Channel.Parent.ID).quotes.Add(e.Args[2]);
                    e.Channel.SendMessage($"<@{e.Author.ID}> Your quote was successfully added to the quotelist!");

                    saveConfig(JsonConvert.SerializeObject(dict));
                }
            }
        }));
    }

    public override void onUnload()
    {
        RemoveCommand("quote");
    }

    private bool isNumeric(object Expression)
    {
        double retNum;

        bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
        return isNum;
    }
}
