﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Objects;
using Nickbot.CommandProvider;
using Nickbot.SampleClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Net;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace Nickbot.SampleClient
{
    class Program
    {
        static Nickbot.Client bot;
        static Timer timer = new Timer();
        static List<string> discordPlaying = new List<string>();
        static int discordPlayingSelected = 0;

        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.WindowWidth + 50, Console.WindowHeight);

            bot = new Nickbot.Client("Nickbot Official Client", "2.0.0");
            using(StreamReader sr = new StreamReader("credentials.txt"))
            {
                bot.InitDiscordClient(true, sr.ReadLine(), sr.ReadLine());

                sr.Close();
            }
            bot.addUrl = "";
            discordPlaying.Add("nickbot.nickstrohm.de");
            discordPlaying.Add("By Nick \"Tiaqo\" Strohm");
            discordPlaying.Add($"{typeof(DiscordClient).Assembly.GetName().Name} {typeof(DiscordClient).Assembly.GetName().Version.ToString()}");
            discordPlaying.Add($"");

            #region events
            bot.GetDiscordClient.AudioPacketReceived += GetDiscordClient_AudioPacketReceived;
            bot.GetDiscordClient.Connected += GetDiscordClient_Connected;
            bot.GetDiscordClient.SocketOpened += GetDiscordClient_SocketOpened;
            #endregion

            bot.StartDiscordClient();

            InputCheck();
        }

        private static void GetDiscordClient_SocketOpened(object sender, EventArgs e)
        {
            bot.LoadModules();
            SetupCommands();
        }

        private static void GetDiscordClient_Connected(object sender, DiscordConnectEventArgs e)
        {
            bot.GetDiscordClient.UpdateCurrentGame(discordPlaying[0], false);
            discordPlayingSelected = 0;

            timer.Interval = 1000 * 10;
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
            timer.Start();


            // API TEST

            //bot.InitClientAPI();

            // API TEST
        }

        private static void SetupCommands()
        {
            /*bot.AddCommand(new CommandStub("update", "Checks if an update is available and download it", "", PermissionType.Owner, e =>
            {
                if (OSDetermination.IsOnMac() || OSDetermination.IsOnUnix())
                {
                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage("Not on unix");
                }
                else
                {
                    HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create($"http://nickbot.nickstrohm.de/update/?v={typeof(Nickbot.SampleClient.Program).Assembly.GetName().Version.ToString()}");
                    httpRequest.Method = "GET";
                    httpRequest.UserAgent = "";
                    httpRequest.AllowAutoRedirect = true;

                    HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();

                    using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        if (sr.ReadToEnd() == "true")
                        {
                            string parameters = "";
                            foreach (string para in Environment.GetCommandLineArgs())
                                parameters = $"{parameters}{para} ";

                            if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                                e.Channel.SendMessage($"<@{e.Author.ID}> New update is available. Downloading...{System.Environment.NewLine}Args: {parameters}");
                            new WebClient().DownloadFile($"http://nickbot.nickstrohm.de/update/?v={typeof(Nickbot.SampleClient.Program).Assembly.GetName().Version.ToString()}&download=true", "update.zip");

                            Process.Start("Nickbot.Update.exe", parameters);
                            Environment.Exit(0);
                        }
                    }
                }
            }));*/
            bot.AddCommand(new CommandStub("servers", "Returns a list of servers the bot is connected to.", "", PermissionType.Owner, e =>
            {
                string tOutput = $"<@{e.Author.ID}>{System.Environment.NewLine}```{System.Environment.NewLine}ServerID:          | Servername:                    | OwnerID:           | Ownername:                    {System.Environment.NewLine}";
                foreach (DiscordServer server in bot.GetDiscordClient.GetServersList())
                {
                    string output = string.Format("{0,-18} | {1,-30} | {2,-18} | {3,-30}", server.ID, server.Name, server.Owner.ID, server.Owner.Username);
                    tOutput = $"{tOutput}{output}{System.Environment.NewLine}";
                }
                tOutput = $"{tOutput}```";
                if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                    e.Channel.SendMessage(tOutput);
            }));
            bot.AddCommand(new CommandStub("servermembers", "Returns a list of users on a specific server", "", PermissionType.Owner, 1, e => {
                string result = "";
                if (e.Args.Count == 0)
                {
                    result = $"{e.Channel.Parent.Members.Count} members on server {e.Channel.Parent.Name}```{System.Environment.NewLine}";
                    result = $"{result}{string.Format("{0,-23} | {1,-30} | {2,-30} | {3,-12}", "MemberID (Discr.):", "Membername:", "Membernickname:", "Botaccount:")}{System.Environment.NewLine}";
                    foreach (DiscordMember member in e.Channel.Parent.Members.Values)
                    {
                        result = $"{result}{string.Format("{0,-23} | {1,-30} | {2,-30} | {3,-12}", $"{member.ID} {member.Discriminator}", member.Username, member.Nickname, member.IsBot)}{System.Environment.NewLine}";
                    }
                    result = $"{result}```";
                    if (result.Length > 2000)
                    {
                        e.Channel.SendMessage($"{e.Channel.Parent.Members.Count} members on server {e.Channel.Parent.Name}```{System.Environment.NewLine}Too much members to list{System.Environment.NewLine}```");
                    } else
                    {
                        e.Channel.SendMessage(result);
                    }
                } else
                {
                    try
                    {
                        DiscordServer server = bot.GetDiscordClient.GetServersList().Find(x => x.ID == e.Args[0]);
                        result = $"{server.Members.Count} members on server {server.Name}```{System.Environment.NewLine}";
                        result = $"{result}{string.Format("{0,-23} | {1,-30} | {2,-30} | {3,-12}", "MemberID (Discr.):", "Membername:", "Membernickname:", "Botaccount:")}{System.Environment.NewLine}";
                        foreach (DiscordMember member in server.Members.Values)
                        {
                            result = $"{result}{string.Format("{0,-23} | {1,-30} | {2,-30} | {3,-12}", $"{member.ID} {member.Discriminator}", member.Username, member.Nickname, member.IsBot)}{System.Environment.NewLine}";
                        }
                        result = $"{result}```";
                        if (result.Length > 2000)
                        {
                            e.Channel.SendMessage($"{server.Members.Count} members on server {server.Name}```{System.Environment.NewLine}Too much member to list{System.Environment.NewLine}```");
                        }
                        else
                        {
                            e.Channel.SendMessage(result);
                        }
                    } catch (Exception ex)
                    {
                        e.Channel.SendMessage($"An error occurred!");
                    }
                }
            }));
            bot.AddCommand(new CommandStub("invite", "Creates a invitelink.", "", PermissionType.Owner, 1, e =>
            {
                if (bot.GetDiscordClient.GetServersList().Find(x => x.ID == e.Args[0]) != null)
                {
                    string inviteUrl = "";

                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.CreateInstanceInvite))
                        inviteUrl = bot.GetDiscordClient.CreateInvite(bot.GetDiscordClient.GetServersList().Find(x => x.ID == e.Args[0]).Channels[0]);

                    if (inviteUrl == "")
                    {
                        if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                            e.Channel.SendMessage($"<@{e.Author.ID}> Could not create an invitelink!");
                    }
                    else
                    {
                        if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                            e.Channel.SendMessage($"<@{e.Author.ID}> {bot.GetDiscordClient.MakeInviteURLFromCode(inviteUrl)}");
                    }
                }
                else
                {
                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage($"<@{e.Author.ID}> Could not find a server with the ID {e.Args[0]}!");
                }
            }));
            /*bot.AddCommand(new CommandStub("style", "Shows a tutorial how to install a custom discord style", "", PermissionType.User, 3, e => {
                if (e.Args.Count == 0)
                {
                    if (bot.HasPermissions(e.Channel, DSharpPlus.Objects.DiscordSpecialPermissions.SendMessages))
                        if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                            e.Channel.SendMessage($@"```javascript
// Open your developer console by pressing CTRL+Shift+I and click on console, then paste the following into the console
// install
iMASCSS = document.createElement(""link"");
iMASCSS.setAttribute(""id"", ""iMASCSS"");
iMASCSS.setAttribute(""rel"", ""stylesheet"");
iMASCSS.setAttribute(""type"", ""text/css"");
document.getElementsByTagName(""head"").item(0).appendChild(iMASCSS);
document.getElementById(""iMASCSS"").setAttribute(""href"", ""https://tools.nickstrohm.de/getStyle.php"");

// uninstall
document.getElementById(""iMASCSS"").setAttribute(""href"", """");
document.getElementsByTagName(""head"").item(0).removeChild(iMASCSS);
delete iMASCSS;
```");
                }
                if (e.Args.Count == 1)
                {
                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage("Dont pass any parameters to get the default colors or pass to colors (CSS-Conform (rgb(a), hex, words))");
                }
                if (e.Args.Count == 2)
                {
                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage($@"```javascript
// Open your developer console by pressing CTRL+Shift+I and click on console, then paste the following into the console
// install
iMASCSS = document.createElement(""link"");
iMASCSS.setAttribute(""id"", ""iMASCSS"");
iMASCSS.setAttribute(""rel"", ""stylesheet"");
iMASCSS.setAttribute(""type"", ""text/css"");
document.getElementsByTagName(""head"").item(0).appendChild(iMASCSS);
document.getElementById(""iMASCSS"").setAttribute(""href"", ""https://tools.nickstrohm.de/getStyle.php?main={e.Args[0]}&darker={e.Args[1]}"");

// uninstall
document.getElementById(""iMASCSS"").setAttribute(""href"", """");
document.getElementsByTagName(""head"").item(0).removeChild(iMASCSS);
delete iMASCSS;
```");
                }
                if (e.Args.Count == 3)
                {
                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage($@"```javascript
// Open your developer console by pressing CTRL+Shift+I and click on console, then paste the following into the console
// install
iMASCSS = document.createElement(""link"");
iMASCSS.setAttribute(""id"", ""iMASCSS"");
iMASCSS.setAttribute(""rel"", ""stylesheet"");
iMASCSS.setAttribute(""type"", ""text/css"");
document.getElementsByTagName(""head"").item(0).appendChild(iMASCSS);
document.getElementById(""iMASCSS"").setAttribute(""href"", ""https://tools.nickstrohm.de/getStyle.php?main={e.Args[0]}&darker={e.Args[1]}&background={e.Args[2]}"");

// uninstall
document.getElementById(""iMASCSS"").setAttribute(""href"", """");
document.getElementsByTagName(""head"").item(0).removeChild(iMASCSS);
delete iMASCSS;
```");
                }
            }));*/
            bot.AddCommand(new CommandStub("gold", "", "", PermissionType.User, 1, e =>
            {
                if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                    e.Channel.SendMessage($"<@{e.Author.ID}> ```fix{System.Environment.NewLine}{e.Args[0]}{System.Environment.NewLine}```");
            }));
            bot.AddCommand(new CommandStub("fancy", "", "", PermissionType.User, 1, e =>
            {
                if (e.Args.Count == 0)
                {
                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                    {
                        e.Channel.SendMessage($"<@{e.Author.ID}> You should provide a text!");
                    }
                }
                else
                {
                    string rawString = e.Args[0];

                    List<char> stuff = new List<char>();
                    string finalString = $"<@{e.Author.ID}> ```";

                    try
                    {
                        foreach (char _char in rawString)
                        {
                            stuff.Add(_char);
                        }
                    }
                    catch (Exception ex) { }

                    for (int i = 0; i < stuff.Count; i++) // Line
                    {
                        foreach (char _char in stuff) // Char
                        {
                            finalString = $"{finalString}{_char} ";
                        }
                        finalString = $"{finalString}{System.Environment.NewLine}";
                        char _temp = stuff[stuff.Count - 1];
                        stuff.RemoveAt(stuff.Count - 1);
                        stuff.Insert(0, _temp);
                    }
                    finalString = $"{finalString}```";

                    if (bot.HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage(finalString);
                }
            }));
        }

        private static void InputCheck()
        {
            string input;
            do
            {
                input = Console.ReadLine();
                if (input.StartsWith("newOwner"))
                {
                    bot.discordOwner = null;
                    bot.discordOwnerString = RandomString(32);
                    Console.WriteLine($"{bot.discordOwnerString}");
                }
                if (input.StartsWith("userlist"))
                {
                    // ┌│├┬└\/
                    string output = $"┌ USERLIST{System.Environment.NewLine}";
                    foreach (DiscordServer server in bot.GetDiscordClient.GetServersList())
                    {
                        if (bot.GetDiscordClient.GetServersList().Last() == server)
                        {
                            output += $"└┬ {server.Name} (by {server.Owner.Username} <@{server.Owner.ID}>){System.Environment.NewLine}";
                            foreach (DiscordMember member in server.Members.Values)
                            {
                                if (member.CurrentVoiceChannel != null)
                                {
                                    if (server.Members.Last().Value == member)
                                    {
                                        output += $" └┬ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                        output += $"  └ {member.CurrentVoiceChannel.Name}{System.Environment.NewLine}";
                                    }
                                    else
                                    {
                                        output += $" ├┬ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                        output += $" │└ {member.CurrentVoiceChannel.Name}{System.Environment.NewLine}";
                                    }
                                }
                                else
                                {
                                    if (server.Members.Last().Value == member)
                                    {
                                        output += $" └ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                    }
                                    else
                                    {
                                        output += $" ├ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                    }
                                }
                            }
                        }
                        else
                        {
                            output += $"├┬ {server.Name} (by {server.Owner.Username}){System.Environment.NewLine}";
                            foreach (DiscordMember member in server.Members.Values)
                            {
                                if (!object.Equals(member.CurrentVoiceChannel, default(DiscordChannel)))
                                {
                                    if (server.Members.Last().Value == member)
                                    {
                                        output += $"│└┬ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                        output += $"│ └ {member.CurrentVoiceChannel.Name}{System.Environment.NewLine}";
                                    }
                                    else
                                    {
                                        output += $"│├┬ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                        output += $"││└ {member.CurrentVoiceChannel.Name}{System.Environment.NewLine}";
                                    }
                                }
                                else
                                {
                                    if (server.Members.Last().Value == member)
                                    {
                                        output += $"│└ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                    }
                                    else
                                    {
                                        output += $"│├ {member.Username} <@{member.ID}>{System.Environment.NewLine}";
                                    }
                                }
                            }
                        }
                    }

                    if (input.Split(new char[] { ' ' }, 2).Count() == 1)
                    {
                        Console.WriteLine(output);
                    }
                    if (input.Split(new char[] { ' ' }, 2).Count() == 2)
                    {
                        using (StreamWriter sw = new StreamWriter(input.Split(new char[] { ' ' }, 2)[1]))
                        {
                            sw.WriteLine(output);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
                if (input.StartsWith("getshadowusers"))
                {
                    List<string> ids = new List<string>();
                    List<DiscordMember> shadows = new List<DiscordMember>();
                    foreach (DiscordServer server in bot.GetDiscordClient.GetServersList())
                    {
                        ids = new List<string>();
                        foreach (DiscordMember member in server.Members.Values)
                        {
                            if (ids.Contains(member.ID))
                            {
                                shadows.Add(member);
                            }
                            else ids.Add(member.ID);
                        }
                    }
                    if (bot.GetDiscordClient.GetServersList()[0].GetMemberByKey(bot.GetDiscordClient.Me.ID) != null)
                    {
                        continue;
                    }

                    if (input.Split(new char[] { ' ' }, 2).Count() == 1)
                    {
                        Console.WriteLine($"Shadowusers: {shadows.Count()}");
                    }
                    if (input.Split(new char[] { ' ' }, 2).Count() == 2)
                    {
                        using (StreamWriter sw = new StreamWriter(input.Split(new char[] { ' ' }, 2)[1]))
                        {
                            sw.WriteLine($"Shadowusers: {shadows.Count()}");
                            foreach (DiscordMember member in shadows)
                            {
                                sw.WriteLine($"{member.Username}#{member.Discriminator} (<@{member.ID}> on {member.Parent.Name} ({member.Parent.ID}) by {member.Parent.Owner.Username}#{member.Parent.Owner.Discriminator} (<@{member.Parent.Owner.ID}>))");
                            }
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
                if (input.StartsWith("exit"))
                {
                    timer.Stop();
                    bot.Exit();
                }

            } while (!string.IsNullOrWhiteSpace(input));
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            discordPlayingSelected += 1;
            if (discordPlayingSelected > (discordPlaying.Count - 1))
                discordPlayingSelected = 0;

            bot.GetDiscordClient.UpdateCurrentGame(discordPlaying[discordPlayingSelected], false);
        }

        private static void GetDiscordClient_AudioPacketReceived(object sender, DSharpPlus.DiscordAudioPacketEventArgs e)
        {
            if (bot.GetDiscordClient.ConnectedToVoice())
                bot.GetDiscordClient.EchoPacket(new DSharpPlus.DiscordAudioPacket(e.OpusAudio));
        }
    }
}
