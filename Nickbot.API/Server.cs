﻿using Grapevine.Server;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Nickbot.API
{
    public class Server
    {
        RESTServer server;
        public Server()
        {
            Config serverConf = new Config()
            {
                Host = "localhost",
                Port = "7951",
                MaxThreads = 4,
                Protocol = "http",
                WebRoot = "/web",
                DirIndex = "index.html"
            };
            server = new RESTServer(serverConf);
            server.Start();

            while(server.IsListening)
            {
                Thread.Sleep(300);
            }
        }

        public void Dispose()
        {
            server.Stop();
            server.Dispose();
            server = null;
        }

        public sealed class MyResource : RESTResource
        {
            [RESTRoute]
            public void HandleAllGetRequests(HttpListenerContext context)
            {
                this.SendTextResponse(context, "GET is a success!");
            }
        }
    }
}
