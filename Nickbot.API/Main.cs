﻿using DiscordSharp;
using DiscordSharp.Commands;
using DiscordSharp.Objects;
using Nickbot;
using Nickbot.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

[ModuleAttributes(
        "name",
        "version",
        "author",
        "desc"
        )]
public class Main : Module
{
    Nickbot.API.Server server;
    public void onLoad()
    {
        server = new Nickbot.API.Server();
    }

    public void onUnload()
    {
        server.Dispose();
        server = null;
    }
}
