﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot.Update
{
    class Program
    {
        static void Main(string[] args)
        {
            string startup = "";
            foreach (string arg in args)
            {
                startup = $"{startup}{arg} ";
                Console.WriteLine(arg);
            }
            startup.Trim();

            Console.WriteLine(startup);

            do
            {
                Console.WriteLine("running");
            } while (IsRunning("Discord Nickbot", true));

            string zipPath = @"update.zip";
            string extractPath = @"\";

            using (ZipFile zip1 = ZipFile.Read(zipPath))
            {
                zip1.ExtractProgress += (sender, e) =>
                {
                    Console.WriteLine($"{e.ArchiveName}/{e.CurrentEntry}");
                };
                foreach (ZipEntry e in zip1)
                {
                    e.Extract(extractPath, ExtractExistingFileAction.OverwriteSilently);
                }
            }

                Process.Start(startup);
            Environment.Exit(0);
        }

        static bool IsRunning(string processName)
        {
            Process[] pName = Process.GetProcessesByName(processName);

            if (pName.Count() > 1)
                return true;

            return false;
        }

        static bool IsRunning(string processName, bool kill)
        {
            bool tReturn = false;

            Process[] pName = Process.GetProcessesByName(processName);

            if (pName.Count() > 1)
            {
                tReturn = true;
                if (kill)
                    foreach (Process proc in pName)
                    {
                        proc.Kill();
                    }
            }

            return tReturn;
        }
    }
}
