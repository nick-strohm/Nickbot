﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nickbot.API
{
    internal class API
    {
        private Client parent;
        private Thread _serverThread;
        private HttpListener _listener;
        private int _port;
        private RESTProvider _provider;

        public int Port
        {
            get { return _port; }
            private set { }
        }

        public API(Client parent)
        {
            this.parent = parent;
            
            this.Initialize(7951);

            this._provider = new RESTProvider();
            this._provider.resources.Add(new RESTProvider.RESTResource()
            {
                endpoint = "",
                module = "",
                needAuth = false,
                responsetype = RESTProvider.RESTMethod.GET
            });
        }

        public void Stop()
        {
            try
            {
                _listener.Stop();
                _serverThread.Abort();
            }
            catch (Exception ex)
            {
                Stop();
                parent.Log("INTERNAL API", new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = ex.Message, TimeStamp = DateTime.Now });
            }
        }

        private void Listen()
        {
            try
            {
                _listener = new HttpListener();
                _listener.Prefixes.Add("http://nickbot." + Dns.GetHostName() + ":" + _port.ToString() + "/api/");
                _listener.Prefixes.Add("http://localhost:" + _port.ToString() + "/api/");
                Console.WriteLine("http://nickbot." + Dns.GetHostName() + ":" + _port.ToString() + "/api/");
                _listener.Start();
                while (_listener.IsListening)
                {
                    try
                    {
                        HttpListenerContext context = _listener.GetContext();
                        Process(context);
                    }
                    catch (Exception ex)
                    {
                        parent.Log("INTERNAL API", new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = ex.Message, TimeStamp = DateTime.Now });
                    }
                }
            }
            catch (Exception ex)
            {
                Stop();
                parent.Log("INTERNAL API", new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = ex.Message, TimeStamp = DateTime.Now });
            }
        }

        private void Process(HttpListenerContext context)
        {
            string path = context.Request.Url.AbsolutePath;
            Console.WriteLine(path);

            JObject _tResponse = new JObject();

            string[] pathParts = path.Split(new char[] { '/' });

            string response = _tResponse.ToString();
            byte[] buffer = Encoding.UTF32.GetBytes(response);
            context.Response.ContentLength64 = buffer.Length;
            Stream output = context.Response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();

            /*
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            string response = "<!doctype HTML><html lang=\"de\"><head><title>API TEST</title></head><body><h1>That worked!</h1><p>" + path + "</p></body></html>";
            byte[] buffer = Encoding.UTF8.GetBytes(response);
            context.Response.ContentLength64 = buffer.Length;
            Stream output = context.Response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();*/
        }

        private void Initialize(int port)
        {
            this._port = port;
            _serverThread = new Thread(this.Listen);
            _serverThread.Start();
            parent.Log("INTERNAL API", new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Debug, Message = $"API Initialized on port {port} in thread {_serverThread.ManagedThreadId}", TimeStamp = DateTime.Now });
        }
    }
}
