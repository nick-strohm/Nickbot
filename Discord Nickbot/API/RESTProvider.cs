﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot.API
{
    class RESTProvider
    {
        public enum RESTMethod
        {
            GET,
            POST,
            PUT,
            PATCH,
            DELETE
        }
        public class RESTResource
        {       
            #region EVENTS
            public event EventHandler<RESTResourceEventArgs> ResourceOpened;
            protected virtual void OnResourceViewed(RESTResourceEventArgs e)
            {
                EventHandler<RESTResourceEventArgs> handler = ResourceOpened;
                if (handler != null)
                {
                    handler(this, e);
                }
            }
            #endregion

            public RESTMethod responsetype { get; internal set; }
            public string module { get; internal set; }
            public string endpoint { get; internal set; }
            public bool needAuth { get; internal set; }
        }

        public List<RESTResource> resources;

        public RESTProvider()
        {
            resources = new List<RESTResource>();
        }
    }

    public class RESTResourceEventArgs : EventArgs
    {
        public DateTime timesamp { get; internal set; }
        public Dictionary<string, string> arguments { get; internal set; }
    }
}
