﻿using Nickbot.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot
{
    public class ModuleWrapper
    {
        public bool canLoad { get; internal set; }

        public string name { get; internal set; }
        public string version { get; internal set; }
        public string author { get; internal set; }
        public string description { get; internal set; }
        public Assembly assembly { get; internal set; }
        private Type type { get; set; }
        public object instance { get; internal set; }
        public bool loaded { get; internal set; }
        public string path { get; internal set; }
        
        public Client parent { get; internal set; }
        public DSharpPlus.DiscordClient discordClient { get; internal set; }
        public DSharpPlus.Objects.DiscordMember owner { get; internal set; }

        public string onMessageHandler = "";

        public ModuleWrapper(Client parent, string path, bool load, DSharpPlus.Objects.DiscordMember owner = null)
        {
            assembly = Assembly.LoadFrom(path);
            try
            {
                type = assembly.GetType($"Main", true);
                var attributes = System.Attribute.GetCustomAttributes(type);
                foreach (var attribute in attributes)
                {
                    if (attribute is ModuleAttributes)
                    {
                        name = ((ModuleAttributes)attribute).name;
                        version = ((ModuleAttributes)attribute).version;
                        author = ((ModuleAttributes)attribute).author;
                        description = ((ModuleAttributes)attribute).description;

                        canLoad = true;
                    } else if (attribute is ModuleHandlerAttributes)
                    {
                        onMessageHandler = ((ModuleHandlerAttributes)attribute).onMessageHandler;
                    }
                }
            }
            catch
            {
                throw new ModuleLoadException("No entrypoint found");
            }

            this.parent = parent;
            this.discordClient = parent.discordClient;
            this.owner = owner;
            this.path = path.Remove(path.Length - 4, 4);

            Console.WriteLine(this.path);

            if (!canLoad)
            {
                ModuleLoadException ex = new ModuleLoadException("The module can not be loaded because the attributes are not complete!");
                ex.Data.Add("modulepath", path);
                throw ex;
            }

            if (load == true)
            {
                loadModule();
            }
        }

        public void loadModule()
        {
            if (this.loaded == true)
                throw new Exception("This module is already loaded!");

            instance = Activator.CreateInstance(type);
            
            instance.GetType().GetProperty("owner").SetValue(instance, owner);
            instance.GetType().GetProperty("parent").SetValue(instance, parent);
            instance.GetType().GetProperty("client").SetValue(instance, discordClient);
            instance.GetType().GetProperty("path").SetValue(instance, path);
            
            instance.GetType().GetMethod("onLoad").Invoke(instance, new object[] { });
            loaded = true;

            Console.WriteLine($"[MODULE] {name} ({version}) loaded");
        }

        public void unloadModule()
        {
            if (this.loaded == false)
                throw new Exception("This module is already unloaded!");

            instance.GetType().GetMethod("onUnload").Invoke(instance, new object[] { });

            instance = null;
            loaded = false;
            
            Console.WriteLine($"[MODULE] {name} ({version}) unloaded");
        }
    }

    public class ModuleLoadException : Exception
    {
        public ModuleLoadException() : base() { }
        public ModuleLoadException(string message) : base(message) { }
        public ModuleLoadException(string message, ModuleLoadException innerException) : base(message, innerException) { }
        public ModuleLoadException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }
}
