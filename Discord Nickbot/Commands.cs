﻿using DSharpPlus;
using DSharpPlus.Commands;
using DSharpPlus.Objects;
using Nickbot;
using Nickbot.CommandProvider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot
{
    class Commands
    {
        public class CommandUses
        {
            public class CommandUsesList
            {
                public string command { get; internal set; }
                public int uses { get; internal set; }
                public DateTime latestUse { get; internal set; }
            }
            public string userId { get; internal set; }
            public List<CommandUsesList> commands { get; internal set; }
        }
        private List<CommandUses> commandSpam = new List<CommandUses>();
        
        public class Command
        {
            public CommandStub command { get; internal set; }
            public int maxUses { get; internal set; }
            public int delay { get; internal set; }
            public bool builtin { get; internal set; }
        }
        private List<Command> commands = new List<Command>();

        private CommandsManager commandManager;
        private Client parent;

        public Commands(DiscordClient client, Client parent)
        {
            commandManager = new CommandsManager(client);
            this.parent = parent;
        }

        public void AddCommand(CommandStub command, int maxUses, int delay)
        {
            if (commands.Find(x => x.command.CommandName == command.CommandName) != null)
                return;

            commands.Add(new Command()
            {
                command = command,
                maxUses = maxUses,
                delay = delay
            });

            commandManager.AddCommand(command);
        }

        internal void OverrideCommands(List<Command> commands)
        {
            this.commands = commands;
        }

        public void RemoveCommand(string command)
        {
            Command _command = commands.Find(x => x.command.CommandName == command);
            if (_command.builtin)
                return;

            commandManager.Commands.Remove(commandManager.Commands.Find(x => x.CommandName == command));
            commands.Remove(_command);
        }

        public void ExecuteCommand(string command, DiscordChannel channel, DiscordMember author)
        {
            parent.Log("INTERNAL COMMAND", new LogMessage() { Level = MessageLevel.Debug, Message = $"{command} {channel.Name} {author.Username}#{author.Discriminator}", TimeStamp = DateTime.Now });
            try
            {
                if (commands.Find(x => x.command.CommandName == command.Split(new char[] { ' ' }, 2)[0]) == null)
                    return;
                /*if (commandSpam.Find(x => x.userId == author.ID) == null)
                    commandSpam.Add(new CommandUses()
                    {
                        userId = author.ID,
                        commands = new List<CommandUses.CommandUsesList>()
                    });
                if (commandSpam.Find(x => x.userId == author.ID).commands.Find(x => x.command == command.Split(new char[] { ' ' }, 2)[0]) == null)
                    commandSpam.Find(x => x.userId == author.ID).commands.Add(new CommandUses.CommandUsesList()
                    {
                        command = command.Split(new char[] { ' ' }, 2)[0],
                        latestUse = DateTime.Now,
                        uses = 0
                    });
                if (commandSpam.Find(x => x.userId == author.ID).commands.Find(x => x.command == command.Split(new char[] { ' ' }, 2)[0]).uses == commands.Find(x => x.command.CommandName == command.Split(new char[] { ' ' }, 2)[0]).maxUses)
                {
                    if (commandSpam.Find(x => x.userId == author.ID).commands.Find(x => x.command == command.Split(new char[] { ' ' }, 2)[0]).latestUse.AddMilliseconds(double.Parse(commands.Find(x => x.command.CommandName == command.Split(new char[] { ' ' }, 2)[0]).delay.ToString())) <= DateTime.Now) {
                        // okay

                    }
                    else
                    {
                        return;
                    }
                }*/

                GetCommandsManager.ExecuteOnMessageCommand(command, channel, author);

                

                /*

                Command _tCommand = commands.Find(x => x.command.CommandName == command);
                CommandUses spam = commandSpam.Find(x => x.userId == author.ID);
                if (spam.commands.Find(x => x.command == command) != null)
                {
                    CommandUses.CommandUsesList _command = spam.commands.Find(x => x.command == command);
                    if (_command.uses >= commands.Find(x => x.command.CommandName == command).maxUses)
                    {
                        if (_command.latestUse.AddMilliseconds(double.Parse(_tCommand.delay.ToString())) <= DateTime.Now)
                        {
                            commandManager.ExecuteOnMessageCommand(command, channel, author);
                            _command.latestUse = DateTime.Now;
                            _command.uses = 1;
                        }
                    }
                    else
                    {
                        commandManager.ExecuteOnMessageCommand(command, channel, author);
                        _command.latestUse = DateTime.Now;
                        _command.uses += 1;
                    }
                }
                else
                {
                    spam.commands.Add(new CommandUses.CommandUsesList()
                    {
                        command = command,
                        latestUse = DateTime.Now,
                        uses = 1
                    });
                    commandManager.ExecuteOnMessageCommand(command, channel, author);
                }*/
            } catch(Exception ex)
            {
                if (parent.HasPermissions(channel, DiscordSpecialPermissions.SendMessages))
                    channel.SendMessage($"An error occured");
                parent.Log("INTERNAL COMMAND", new LogMessage() { Level = MessageLevel.Critical, Message = $"{command.Split(new char[] { ' ' })[0]}: {ex.Message}", TimeStamp = DateTime.Now });
            }
        }

        internal List<Command> GetCommands
        {
            get
            {
                return commands;
            }
        }

        internal void SetBuiltin(string command, bool builtin)
        {
            commands.Find(x => x.command.CommandName == command).builtin = builtin;
        }

        internal void SetBuiltin(bool builtin)
        {
            commands.Last().builtin = builtin;
        }

        internal CommandsManager GetCommandsManager
        {
            get
            {
                return commandManager;
            }
        }
    }
}
