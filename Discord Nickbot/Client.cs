﻿using DSharpPlus;
using DSharpPlus.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using Nickbot.CommandProvider;

namespace Nickbot
{
    public class Client
    {
        public class ModuleProvider
        {
            [JsonProperty("modulePath")]
            public string modulePath { get; internal set; }
            [JsonProperty("loadModule")]
            public bool loadModule { get; internal set; }
        }
        internal List<ModuleProvider> moduleInformations;
        internal List<ModuleWrapper> modules = new List<ModuleWrapper>();
        internal bool modulesWasLoaded = false;

        public class PrefixProvider
        {
            [JsonProperty("serverId")]
            public string serverId { get; internal set; }
            [JsonProperty("prefix")]
            public string prefix { get; internal set; }
        }
        internal List<PrefixProvider> prefixes = new List<PrefixProvider>();

        internal DiscordClient discordClient;
        internal CommandsManager discordCommandsManager;
        public DiscordMember discordOwner;
        public string discordOwnerString = "";
        internal Commands commands;

        internal API.API nickbotAPI;

        internal DateTime runningSince;
        internal bool runningOnMono = false;
        internal string osString;
        public string clientName;
        public string clientVersion;
        public string addUrl;
        
        public Client(string clientName, string clientVersion)
        {
            runningSince = DateTime.Now;

            runningOnMono = Type.GetType("Mono.Runtime") != null;
            if (OSDetermination.IsOnUnix())
                osString = OSDetermination.GetUnixName();
            else
                osString = Environment.OSVersion.ToString();

            this.clientName = clientName;
            this.clientVersion = clientVersion;
        }

        /// <summary>
        /// Loads the modules in the folder modules/
        /// </summary>
        public void LoadModules()
        {
            if (!Directory.Exists("modules/")) Directory.CreateDirectory("modules/");
            if (File.Exists("modules.json"))
                moduleInformations = JsonConvert.DeserializeObject<List<ModuleProvider>>(File.ReadAllText("modules.json"));
            if (moduleInformations == null)
                moduleInformations = new List<ModuleProvider>();

            foreach (string modulePath in Directory.GetFiles("modules/"))
            {
                try
                {
                    if (!modulePath.EndsWith(".dll"))
                        continue;

                    if (moduleInformations.Find(x => x.modulePath == modulePath) == null)
                    {
                        modules.Add(new ModuleWrapper(this, modulePath, false, discordOwner));

                        moduleInformations.Add(new ModuleProvider() { loadModule = false, modulePath = modulePath });
                    }
                    else if (moduleInformations.Find(x => x.modulePath == modulePath) != null)
                    {
                        modules.Add(new ModuleWrapper(this, modulePath, moduleInformations.Find(x => x.modulePath == modulePath).loadModule, discordOwner));
                    }

                }
                catch (ModuleLoadException ex)
                {
                    if (ex.Message == "No entrypoint found")
                        continue;
                    Console.WriteLine($"Error while loading module \"{ex.Data["modulepath"]}\": {ex.Message}");
                }
            }

            File.WriteAllText("modules.json", JsonConvert.SerializeObject(moduleInformations));

            modulesWasLoaded = true;
        }

        public void UnloadModules()
        {
            foreach (ModuleWrapper wrapper in modules)
            {
                try
                {
                    if (wrapper.loaded)
                        wrapper.unloadModule();
                }
                catch (Exception ex)
                {
                    Log("INTERNAL", new LogMessage() { Level = MessageLevel.Warning, TimeStamp = DateTime.Now, Message = $"Could not unload module \"{wrapper.name}\": {ex.Message}" });
                }
            }
        }

        #region DISCORD
        /// <summary>
        /// 
        /// </summary>
        /// <param name="isBotAccount"></param>
        /// <param name="user"></param>
        /// <param name="pass"></param>
        public void InitDiscordClient(bool isBotAccount, string user, string pass)
        {
            if (isBotAccount)
                discordClient = new DiscordClient(pass, true, true);
            else
            {
                discordClient = new DiscordClient(DSharpPlus.Toolbox.Tools.getUserToken(user, pass), false, true);
            }
            discordClient.GetTextClientLogger.EnableLogging = true;
            discordClient.GetTextClientLogger.LogMessageReceived += (sender, e) => Log("DEBUG", e.message);

            SetupCommands();

            #region events
            discordClient.Connected += (sender, e) => {
                Log("discord", new LogMessage() { Level = MessageLevel.Unecessary, TimeStamp = DateTime.Now, Message = $"Connected! User {e.User.Username}" });
                Log("discord", new LogMessage() { Level = MessageLevel.Unecessary, TimeStamp = DateTime.Now, Message = $"Connected to {discordClient.CurrentGatewayURL} (v{discordClient.DiscordGatewayVersion})" });

                System.Threading.Thread.Sleep(300);
                
                if (File.Exists("permissions.json"))
                {
                    Dictionary<string, PermissionType> permissionsDictionary = JsonConvert.DeserializeObject<Dictionary<string, PermissionType>>(File.ReadAllText("permissions.json"));
                    if (permissionsDictionary == null)
                        permissionsDictionary = new Dictionary<string, PermissionType>();
                    if (permissionsDictionary.Count == 0 && discordOwner != null)
                        permissionsDictionary.Add(discordOwner.ID, PermissionType.Owner);

                    commands.GetCommandsManager.OverridePermissionsDictionary(permissionsDictionary);
                }
            };
            discordClient.MessageReceived += (sender, e) =>
            {
                if (e.Author.ID == discordClient.Me.ID)
                    return;

                string prefix = "nBot|";
                if (prefixes.Find(x => x.serverId == e.Channel.Parent.ID) != null)
                    prefix = prefixes.Find(x => x.serverId == e.Channel.Parent.ID).prefix;

                string message = e.MessageText.TrimEnd(new char[] { ' ' });
                if (message.StartsWith($"{prefix}owner"))
                {
                    string[] messageParts = message.Split(new char[] { ' ' }, 2);
                    if (messageParts.Count() < 2)
                    {
                        string ownerID = "nobody";
                        if (discordOwner != null)
                            ownerID = $"<@{discordOwner.ID}>";

                        if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                            e.Channel.SendMessage($"<@{e.Author.ID}> My current owner is {ownerID}");
                    }
                    else
                    {
                        if (discordOwnerString != "" && messageParts[1] == discordOwnerString)
                        {
                            discordOwner = e.Author;
                            discordOwnerString = "";
                            File.WriteAllText("owner.txt", discordOwner.ID);
                            commands.GetCommandsManager.AddPermission(e.Author, PermissionType.Owner);
                            if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                                e.Channel.SendMessage($"<@{e.Author.ID}> is now my owner!");
                        }
                        else
                        {
                            if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                                e.Channel.SendMessage($"<@{e.Author.ID}> your provided code is not correct! Please enter ``newOwner`` into your console");
                        }
                    }
                }
                else if (message == "nBot|about")
                {
                    commands.GetCommandsManager.ExecuteOnMessageCommand("about", e.Channel, e.Author);
                }
                else
                {
                    if (message.Length > 0 && message.StartsWith($"{prefix}"))
                    {
                        string rawCommand = message.Remove(0, prefix.Length);
                        try
                        {
                            //commands.GetCommandsManager.ExecuteOnMessageCommand(rawCommand, e.Channel, e.Author);
                            commands.ExecuteCommand(rawCommand, e.Channel, e.Author);
                        }
                        catch (UnauthorizedAccessException ex)
                        {
                            e.Channel.SendMessage(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            e.Channel.SendMessage($"Exception occurred while running command:{System.Environment.NewLine}```{ex.Message}{System.Environment.NewLine}{ex.StackTrace}```");
                        }
                    }
                }
            };
            discordClient.SocketClosed += (sender, e) =>
            {
                if (!e.WasClean)
                {
                    Log("DISCORD", new LogMessage() { Level = MessageLevel.Critical, TimeStamp = DateTime.Now, Message = $"[Clean {e.WasClean} | Code {e.Code}] {e.Reason}" });
                    Exit(true);
                    InitDiscordClient(isBotAccount, user, pass);

                    if (modulesWasLoaded)
                        LoadModules();
                }
            };
            discordClient.GuildAvailable += (sender, e) =>
            {
                if (discordOwner == null && File.Exists("owner.txt"))
                {
                    try
                    {
                        discordOwner = discordClient.GetServersList().Find(x => x.Members.First(y => y.Value.ID == File.ReadAllText("owner.txt")).Value != null)?.Members.First(x => x.Value.ID == File.ReadAllText("owner.txt")).Value;
                    }
                    catch {  }

                }
            };
            #endregion
        }

        public void StartDiscordClient(bool useDotNetWebsocket = false)
        {
            if (discordClient.SendLoginRequest() != null)
                discordClient.Connect(false);
        }

        /// <summary>
        /// 
        /// </summary>
        public DiscordClient GetDiscordClient
        {
            get
            {
                return discordClient;
            }
            set
            {

            }
        }

        #region Commands
        /// <summary>
        /// Adds a command to the commandlist
        /// </summary>
        /// <param name="command"></param>
        public void AddCommand(CommandStub command)
        {            
            commands.AddCommand(command, 1, 1000 * 60);
        }
        public void AddCommand(CommandStub command, int maxUses, int delay)
        {            
            commands.AddCommand(command, maxUses, delay);
        }

        /// <summary>
        /// Removes a command from the commandlist
        /// </summary>
        /// <param name="commandName"></param>
        public void RemoveCommand(string commandName)
        {
            commands.RemoveCommand(commandName);
        }

        public void AddPermission(DiscordMember member, PermissionType type)
        {
            commands.GetCommandsManager.AddPermission(member, type);
        }
        #endregion

        /// <summary>
        /// Checks if you have a specific permission on the server or in the channel
        /// </summary>
        /// <param name="CurrentChannel"></param>
        /// <param name="permission"></param>
        /// <returns>Returns true if you have the specific permission</returns>
        public bool HasPermissions(DiscordChannel currentChannel, DiscordSpecialPermissions permission)
        {
            DiscordServer current = currentChannel.Parent;
            DiscordMember me = current.GetMemberByKey(discordClient.Me.ID);

            foreach (var role in me.Roles)
            {
                if (role.Permissions.HasPermission(permission))
                {
                    return true;
                }
            }
            foreach (var perm in currentChannel.PermissionOverrides)
            {
                if (perm.AllowedPermission(permission))
                {
                    return true;
                }
            }
            return false;
        }
        
        private void SetupCommands()
        {
            commands = new Commands(discordClient, this);

            //Owner
            AddCommand(new CommandStub("exit", "Stops the bot.", "", PermissionType.Owner, e =>
            {
                Exit();
            }));
            commands.SetBuiltin(true);
            AddCommand(new CommandStub("restart", "Restarts the bot.", "", PermissionType.Owner, e =>
            {
                if (OSDetermination.IsOnUnix())
                {
                    if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage($"Not on unix!");
                    return;
                }

                string parameters = "";
                foreach (string para in Environment.GetCommandLineArgs())
                    parameters = $"{parameters}{para} ";

                Process.Start(parameters);
                Exit();
            }));
            commands.SetBuiltin(true);
            AddCommand(new CommandStub("setperm", "Sets the rank of a specific user", $"setperm <rank> <user>{System.Environment.NewLine}Ranks:{System.Environment.NewLine}Admin Mod User None", PermissionType.Owner, 2, e =>
            {
                if (e.Args.Count > 1)
                {
                    string permString = e.Args[0];
                    PermissionType type = PermissionType.User;
                    switch (permString.ToLower())
                    {
                        case "admin":
                            type = PermissionType.Admin;
                            break;
                        case "mod":
                            type = PermissionType.Mod;
                            break;
                        case "none":
                            type = PermissionType.None;
                            break;
                        case "user":
                            type = PermissionType.User;
                            break;
                    }
                    string id = e.Args[1].Trim(new char[] { '<', '@', '>' });
                    commands.GetCommandsManager.AddPermission(id, type);
                    e.Channel.SendMessage($"Given permission {type.ToString().Substring(type.ToString().IndexOf('.') + 1)} to <@{id}>!");
                }
            }));
            commands.SetBuiltin(true);
            AddCommand(new CommandStub("module", "Manages the modules.", "", PermissionType.Owner, 2, e =>
            {
                if (e.Args.Count < 1)
                {
                    string permString = $"<@{e.Author.ID}>{System.Environment.NewLine}The current modules:{System.Environment.NewLine}";
                    foreach (ModuleWrapper module in modules)
                    {
                        if (module.loaded)
                            permString = $"{permString}[LOADED] ";
                        else
                            permString = $"{permString}[UNLOADED] ";
                        permString = $"{permString}**{module.name} ({module.version})** _(by {module.author})_ : {module.description}{System.Environment.NewLine}";
                    }
                    e.Channel.SendMessage(permString);
                }
                else if (e.Args.Count > 1)
                {
                    switch (e.Args[0])
                    {
                        case "load":
                            if (modules.Find(x => x.name == e.Args[1]) != null)
                            {
                                ModuleWrapper module = modules.Find(x => x.name == e.Args[1]);
                                if (module.loaded)
                                    e.Channel.SendMessage($"<@{e.Author.ID}> The module ``{e.Args[1]}`` is already loaded!");
                                else
                                {
                                    module.loadModule();
                                    e.Channel.SendMessage($"<@{e.Author.ID}> The module ``{e.Args[1]}`` was loaded!");
                                }
                            }
                            else
                                e.Channel.SendMessage($"<@{e.Author.ID}> The module ``{e.Args[1]}`` was not found!");
                            break;
                        case "unload":
                            if (modules.Find(x => x.name == e.Args[1]) != null)
                            {
                                ModuleWrapper module = modules.Find(x => x.name == e.Args[1]);
                                if (!module.loaded)
                                    e.Channel.SendMessage($"<@{e.Author.ID}> The module ``{e.Args[1]}`` is already unloaded!");
                                else
                                {
                                    module.unloadModule();
                                    e.Channel.SendMessage($"<@{e.Author.ID}> The module ``{e.Args[1]}`` was unloaded!");
                                }
                            }
                            else
                                e.Channel.SendMessage($"<@{e.Author.ID}> The module with the name ``{e.Args[1]}`` was not found!");
                            break;
                    }
                }
            }));
            commands.SetBuiltin(true);
            //Admin
            AddCommand(new CommandStub("prefix", "Manages the prefix for the current server. Serverowner only", "Use remove or set", PermissionType.User, 2, e =>
            {
                string prefix = "nBot|";
                if (prefixes.Find(x => x.serverId == e.Channel.Parent.ID) != null)
                    prefix = prefixes.Find(x => x.serverId == e.Channel.Parent.ID).prefix;
                if (e.Args.Count < 1)
                    e.Channel.SendMessage($"<@{e.Author.ID}> The prefix for the current server is ``{prefix}``");
                else
                {
                    if (e.Author.ID != e.Channel.Parent.Owner.ID || !discordCommandsManager.HasPermission(e.Author, PermissionType.Owner)) return;

                    switch (e.Args[0])
                    {
                        case "remove":
                            if (prefixes.Find(x => x.serverId == e.Channel.Parent.ID) != null)
                            {
                                e.Channel.SendMessage($"<@{e.Author.ID}> The prefix ``{prefix}`` was removed!");
                                prefixes.Remove(prefixes.Find(x => x.serverId == e.Channel.Parent.ID));

                                using (StreamWriter sw = new StreamWriter("prefixes.txt", false))
                                {
                                    sw.Write(JsonConvert.SerializeObject(prefixes));
                                    sw.Flush();
                                    sw.Close();
                                }
                            }
                            break;
                        case "set":
                            if (prefixes.Find(x => x.serverId == e.Channel.Parent.ID) != null)
                            {
                                e.Channel.SendMessage($"<@{e.Author.ID}> The prefix was changed from ``{prefix}`` to ``{e.Args[1]}``");
                                prefixes.Find(x => x.serverId == e.Channel.Parent.ID).prefix = e.Args[1];

                                using (StreamWriter sw = new StreamWriter("prefixes.txt", false))
                                {
                                    sw.Write(JsonConvert.SerializeObject(prefixes));
                                    sw.Flush();
                                    sw.Close();
                                }
                            }
                            else
                            {
                                e.Channel.SendMessage($"<@{e.Author.ID}> The prefix for the current server is now ``{e.Args[1]}``");
                                prefixes.Add(new PrefixProvider() { serverId = e.Channel.Parent.ID, prefix = e.Args[1] });

                                using (StreamWriter sw = new StreamWriter("prefixes.txt", false))
                                {
                                    sw.Write(JsonConvert.SerializeObject(prefixes));
                                    sw.Flush();
                                    sw.Close();
                                }
                            }
                            break;
                    }
                }
            }));
            commands.SetBuiltin(true);
            //Mod

            //User
            AddCommand(new CommandStub("about", "Returns information about the bot", "", PermissionType.User, e => {
                string ownerId = "nobody";
                if (discordOwner != null)
                    ownerId = $"<@{discordOwner.ID}>";

                string prefix = "nBot|";
                if (prefixes.Find(x => x.serverId == e.Channel.Parent.ID) != null)
                    prefix = prefixes.Find(x => x.serverId == e.Channel.Parent.ID).prefix;

                string format = $@"{{0}}
**__*{{15}}*__**
**Current name:** {{1}}
**Version:** {{2}}
**Owner:** {{3}}
**Libraries:** {{4}}
**Runtime:** {{5}}
**OS:** {{6}}
**Prefix:** {{7}}
**Running since:** {{8}}
**Total servers:** {{9}}
**Memoryusage:** {{10}}
**Modules (Loaded/Total):** {{13}}
**Commands:** {{14}}
**Add me to your server:** {{11}}
**Get help:** {{12}}";

                TimeSpan tTime = DateTime.Now - runningSince;

                string runtime;
                if (runningOnMono)
                    runtime = "Mono";
                else
                    runtime = ".Net";

                Process.GetCurrentProcess().Refresh();

                if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                    e.Channel.SendMessage(string.Format(format, $"<@{e.Author.ID}>", discordClient.Me.Username, clientVersion, ownerId,
                        $@"
    {typeof(Client).Assembly.GetName().Name} {typeof(Client).Assembly.GetName().Version.ToString()} Nick Strohm ( https://gitlab.com/nick-strohm/nickbot )
    {typeof(DiscordClient).Assembly.GetName().Name} {typeof(DiscordClient).Assembly.GetName().Version.ToString()} by NaamloosDT (, Axiom and contributors) ( https://github.com/NaamloosDT/DSharpPlus )",
                        runtime, osString, prefix, $"{tTime.Days} Days, {tTime.Hours} Hours, {tTime.Minutes} Minutes", discordClient.GetServersList().Count, $"{(Process.GetCurrentProcess().WorkingSet64 / 1024) / 1024}MB",
                        addUrl, "https://discord.gg/0xv4CVV6dLofsEiB", $"{modules.FindAll(x => x.loaded == true).Count}/{modules.Count}",
                        commands.GetCommands.Count, clientName));
            }));
            commands.SetBuiltin(true);
            AddCommand(new CommandStub("cmdlist", "Lists all commands.", "", PermissionType.User, 1, e =>
            {
                int commandsPerPage = 10;
                int page = 1;
                int maxPages = (int)Math.Ceiling((double)commands.GetCommandsManager.Commands.Count / commandsPerPage);
                List<ICommand> _tCommands = commands.GetCommandsManager.Commands.OrderBy(o => o.CommandName).OrderBy(o => o.MinimumPermission).ToList();

                if (e.Args.Count() == 0)
                {
                    page = 1;
                }
                else
                {
                    page = int.Parse(e.Args[0]);
                }

                string result = $"<@{e.Author.ID}> Commandlist ({page}/{maxPages}):```{System.Environment.NewLine}{string.Format("{0,-5} | {1,-15} | {2}", "Perm.", "Command", "Description")}{System.Environment.NewLine}";

                for (int i = ((page - 1)*commandsPerPage); i < ((page)*commandsPerPage); i++)
                {
                    try
                    {
                        ICommand command = _tCommands[i];
                        result = $"{result}{string.Format("{0,-5} | {1,-15} | {2}", command.MinimumPermission.ToString().ToUpper(), command.CommandName, command.Description)}{System.Environment.NewLine}";
                    }
                    catch (Exception ex) { }
                }

                result = $"{result}```";

                e.Channel.SendMessage(result);
            }));
            commands.SetBuiltin(true);
            AddCommand(new CommandStub("help", "Displays help for a specific command.", "", PermissionType.User, 1, e =>
            {
                if (e.Args.Count > 0)
                {
                    if (commands.GetCommandsManager.Commands.Find(x => x.CommandName == e.Args[0]) != null)
                    {
                        ICommand command = commands.GetCommandsManager.Commands.Find(x => x.CommandName == e.Args[0]);
                        e.Channel.SendMessage($"<@{e.Author.ID}> [{command.MinimumPermission.ToString().ToUpper()}] **{command.CommandName}**: {command.Description}{System.Environment.NewLine}```{System.Environment.NewLine}{command.HelpTag}{System.Environment.NewLine}```");
                    }
                    else
                    {
                        e.Channel.SendMessage($"<@{e.Author.ID}> Could not find the command ``{e.Args[0]}``!");
                    }
                }
                else
                    e.Channel.SendMessage($"<@{e.Author.ID}> You have to provide a command.");
            }));
            commands.SetBuiltin(true);
            AddCommand(new CommandStub("owner", "", "", PermissionType.User, 1, e =>
            {
                if (e.Args.Count() == 0)
                {
                    string ownerID = "nobody";
                    if (discordOwner != null)
                        ownerID = $"<@{discordOwner.ID}>";

                    if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                        e.Channel.SendMessage($"<@{e.Author.ID}> My current owner is {ownerID}");
                }
                else
                {
                    if (discordOwnerString != "" && e.Args[0] == discordOwnerString)
                    {
                        discordOwner = e.Author;
                        discordOwnerString = "";
                        File.WriteAllText("owner.txt", discordOwner.ID);
                        AddPermission(discordOwner, PermissionType.Owner);
                        if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                            e.Channel.SendMessage($"<@{discordOwner.ID}> you are now my owner!");
                    }
                    else
                    {
                        if (HasPermissions(e.Channel, DiscordSpecialPermissions.SendMessages))
                            e.Channel.SendMessage($"<@{discordOwner.ID}> your provided code is not corrent! Please enter newOwner into your console");
                    }
                }
            }));
            commands.SetBuiltin(true);
        }

        #endregion

        #region API
        public void InitClientAPI()
        {
            nickbotAPI = new API.API(this);
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="level"></param>
        /// <param name="message"></param>
        public void Log(string app, LogMessage m)
        {
            if (!File.Exists("nickbot.log"))
                File.WriteAllText("nickbot.log", "");
            if (m.Level == MessageLevel.Unecessary)
                return;
            switch (m.Level)
            {
                case MessageLevel.Debug:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case MessageLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Critical:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.BackgroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
            }
            Console.Write($"[{app}: {m.TimeStamp}:{m.TimeStamp.Millisecond}]: ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(m.Message + "\n");
            Console.BackgroundColor = ConsoleColor.Black;

            //File.AppendAllText("nickbot.log", $"{m.TimeStamp} | [{app.ToUpper()}] {m.Level} -- {m.Message}{System.Environment.NewLine}");
        }

        [Obsolete]
        public void Log(string app, MessageLevel level, string message)
        {
            Log(app, new LogMessage() { Level = level, TimeStamp = DateTime.Now, Message = message });
        }

        public List<PrefixProvider> GetPrefixes
        {
            get
            {
                return prefixes;
            }
            set
            {
                
            }
        }

        public void Exit(bool withoutStop = false)
        {
            if (CommandsManager.UserRoles != null && CommandsManager.UserRoles.Count > 0)
                File.WriteAllText("permissions.json", JsonConvert.SerializeObject(CommandsManager.UserRoles));

            discordClient.Logout();
            discordClient.Dispose();
            discordClient = null;
            discordCommandsManager = null;
            UnloadModules();
            if (!withoutStop)
                Environment.Exit(0);
        }
    }
}
