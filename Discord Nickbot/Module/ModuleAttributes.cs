﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot.Module
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                           System.AttributeTargets.Struct,
                           AllowMultiple = true)
    ]
    public class ModuleAttributes : System.Attribute
    {
        public string name { get; internal set; }
        public string version { get; internal set; }
        public string author { get; internal set; }
        public string description { get; internal set; }

        public ModuleAttributes(string name, string version, string author, string description)
        {
            this.name = name;
            this.version = version;
            this.author = author;
            this.description = description;
        }
    }
}
