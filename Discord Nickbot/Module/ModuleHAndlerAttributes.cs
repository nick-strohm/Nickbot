﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot.Module
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                           System.AttributeTargets.Struct,
                           AllowMultiple = true)
    ]
    public class ModuleHandlerAttributes : System.Attribute
    {
        public string onMessageHandler { get; internal set; }

        public ModuleHandlerAttributes(string onMessageHandler)
        {
            this.onMessageHandler = onMessageHandler;
        }
    }
}
