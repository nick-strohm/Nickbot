﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nickbot.Module
{
    public class Module
    {
        public string name { get; internal set; }
        public string version { get; internal set; }
        public string path { get; internal set; }

        public DSharpPlus.Objects.DiscordMember owner { get; internal set; }
        public DSharpPlus.DiscordClient client { get; internal set; }
        public Client parent { private get; set; }

        public Module()
        {
            var attributes = System.Attribute.GetCustomAttributes(this.GetType());
            if (attributes[0] is ModuleAttributes)
            {
                this.name = ((ModuleAttributes)attributes[0]).name;
                this.version = ((ModuleAttributes)attributes[0]).version;
            }
        }

        public virtual void onLoad() {  }

        public virtual void onUnload() {  }

        public virtual string createConfig() { return ""; }

        public void Log(DSharpPlus.LogMessage message)
        {
            parent.Log($"MODULE] [{name}", message);
        }

        public void AddCommand(CommandProvider.CommandStub command) { parent.AddCommand(command); }

        public void RemoveCommand(string commandName) { parent.RemoveCommand(commandName); }
        
        public bool HasPermissions(DSharpPlus.Objects.DiscordChannel currentChannel, DSharpPlus.Objects.DiscordSpecialPermissions permission) { return parent.HasPermissions(currentChannel, permission); }

        public string loadConfig()
        {
            if (!File.Exists($"{path}.json"))
                File.WriteAllText($"{path}.json", createConfig());

            return File.ReadAllText($"{path}.json");
        }

        public void saveConfig<T>(T config) where T : JToken, new()
        {
            File.WriteAllText($"{path}.json", config.ToString());
        }

        public void saveConfig(string config)
        {
            File.WriteAllText($"{path}.json", config);
        }

        public void saveConfig()
        {
            File.WriteAllText($"{path}.json", createConfig());
        }
    }
}
