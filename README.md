# Nickbot [![Build status](https://ci.appveyor.com/api/projects/status/u43790kq4l4577xi/branch/master?svg=true)](https://ci.appveyor.com/project/nick-strohm/nickbot/branch/master)

## Download: Coming soon

Nickbot is a simple Discord-Bot based on DSharpPlus by [NaamloosDT](https://www.github.com/NaamloosDT)

To get Nickbot running, you have to create a ``credentials.txt``.

Content:
```
Discord-Email (Bot-User)
Discord-Passwort (Bot-Secret)
```

##### Used libraries:
- [NaamloosDT](https://www.github.com/NaamloosDT) [DSharpPlus](https://www.github.com/NaamloosDT/DSharpPlus)
- [Newtonsofts JSON.NET](http://www.newtonsoft.com/json)