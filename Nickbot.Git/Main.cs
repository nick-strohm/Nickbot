﻿using DSharpPlus.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nickbot;
using Nickbot.CommandProvider;
using Nickbot.Git.Brain;
using Nickbot.Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[ModuleAttributes("Git", "0.1", "Nick 'Tiaqo' Strohm", "")]
public class Main : Module
{
    private GitBrain brain;

    public List<Repo> dict = new List<Repo>();

    public string githubToken = "";
    public string gitlabToken = "";

    public override void onLoad()
    {
        JObject settings = JObject.Parse(loadConfig());
        foreach (JObject JRepo in settings["arr"])
        {
            GitProvider provider = JRepo["Provider"].ToObject<GitProvider>();
            string repository = JRepo["Repository"].ToString();
            string[] branches = JRepo["Branches"].ToObject<string[]>();
            long channelId = JRepo["ChannelID"].ToObject<long>();
            DateTimeOffset lastUpdate;
            if (!DateTimeOffset.TryParse(JRepo["LastUpdate"].ToString(), out lastUpdate))
            {
                lastUpdate = DateTimeOffset.UtcNow;
            }
            Repo repo = new Repo(provider, repository, channelId)
            {
                Branches = branches,
                LastUpdate = lastUpdate
            };
            dict.Add(repo);
        }

        githubToken = settings["token"]["githubToken"].ToString();
        gitlabToken = settings["token"]["gitlabToken"].ToString();
        
        Log(new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = $"Hub: {githubToken} Lab: {gitlabToken}", TimeStamp = DateTime.Now });
        Log(new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = $"{dict.Count} repos loaded", TimeStamp = DateTime.Now });
        Log(new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = $"    {dict.Where(x => x.Provider == GitProvider.Github).Count()} repos from github", TimeStamp = DateTime.Now });
        Log(new DSharpPlus.LogMessage() { Level = DSharpPlus.MessageLevel.Critical, Message = $"    {dict.Where(x => x.Provider == GitProvider.Gitlab).Count()} repos from gitlab", TimeStamp = DateTime.Now });

        AddCommand(new CommandStub("repos", "", "", PermissionType.Owner, 4, (e) =>
        {
            Console.WriteLine($"{e.Args.Count}");
            Console.WriteLine($"Repos: {dict.Count}");
            switch (e.Args[0])
            {
                case "list":
                    StringBuilder builder = new StringBuilder();

                    foreach (var repo in dict.OrderBy(x => x.Repository))
                        builder.AppendLine($"{repo.Repository} [{string.Join(",", repo.Branches)}] => {client.GetChannelByID(repo.ChannelID)?.Name ?? "Unknown"}");
                    e.Channel.SendMessage(builder.ToString());
                    break;
                case "add":
                    if (e.Args.Count >= 3)
                    {
                        GitProvider provider;
                        if (Enum.TryParse(e.Args[1], out provider))
                        {
                            string repo = e.Args[2];
                            long channelid;
                            DiscordChannel channel;
                            if (e.Args.Count >= 4 && e.Args[3] != null && e.Args[3] != "" && long.TryParse(e.Args[3], out channelid))
                            {
                                channel = client.GetChannelByID(channelid);
                            }
                            else
                                channel = e.Channel;

                            string url = FilterUrl(e.Args[2]);
                            AddRepo(provider, url, long.Parse(channel.ID));
                            saveConfig(createConfig());
                            brain.UpdateRepos(dict);
                            e.Channel.SendMessage($"Added repo {url} [{provider.ToString()}] to {channel.Name}");
                        }
                    }
                    break;
                case "remove":
                    if (e.Args.Count >= 3)
                    {
                        GitProvider provider;
                        if (Enum.TryParse(e.Args[1], out provider))
                        {
                            string repo = e.Args[2];
                            string url = FilterUrl(repo);
                            RemoveRepo(provider, url);
                            saveConfig(createConfig());
                            brain.UpdateRepos(dict);
                            e.Channel.SendMessage($"Removed repo {url} [{provider.ToString()}]");
                        }
                    }
                    break;
                case "addbranch":
                    if (e.Args.Count >= 4)
                    {
                        GitProvider provider;
                        if (Enum.TryParse(e.Args[1], out provider))
                        {
                            string url = FilterUrl(e.Args[2]);
                            var repo = dict.First(x => x.Provider == provider && x.Repository == url);
                            if (repo != null)
                            {
                                repo.AddBranch(e.Args[3]);
                                saveConfig(createConfig());
                                brain.UpdateRepos(dict);
                                e.Channel.SendMessage($"Added branch {url}/{e.Args[3]} [{provider.ToString()}]");
                            }
                        }
                    }
                    break;
                case "removebranch":
                    if (e.Args.Count >= 4)
                    {
                        GitProvider provider;
                        if (Enum.TryParse(e.Args[1], out provider))
                        {
                            string url = FilterUrl(e.Args[2]);
                            var repo = dict.Find(x => x.Provider == provider && x.Repository == url);
                            if (repo != null)
                            {
                                repo.RemoveBranch(e.Args[3]);
                                saveConfig(createConfig());
                                brain.UpdateRepos(dict);
                                e.Channel.SendMessage($"Removed branch {url}/{e.Args[3]} [{provider.ToString()}]");
                            }
                        }
                    }
                    break;
                case null:
                default:
                    break;
            }
        }));

        brain = new GitBrain(dict, githubToken, gitlabToken);
        brain.MessageReceived += (sender, e) =>
        {
            Console.WriteLine($"Received message from GitBrain");
            var channel = client.GetChannelByID(e.Repository.ChannelID);
            channel.SendMessage(e.Message);
        };
        brain.RepositoryUpdated += (sender, e) =>
        {
            Console.WriteLine($"Received update from GitBrain");
            var repo = dict.Find(x => x.Provider == e.Repository.Provider && x.Repository == e.Repository.Repository && x.ChannelID == e.Repository.ChannelID);
            if (repo != null)
                repo = e.Repository;
            saveConfig();
        };
        brain.Start();
    }
    
    public override void onUnload()
    {
        brain.Stop();
        RemoveCommand("repos");
        saveConfig(createConfig());
    }

    public override string createConfig()
    {
        return new JObject()
        {
            {
                "token", new JObject()
                {
                    { "githubToken", githubToken },
                    { "gitlabToken", gitlabToken }
                }
            },
            { "arr", JArray.Parse(JsonConvert.SerializeObject(dict)) }
        }.ToString();
    }

    private static string FilterUrl(string url)
    {
        if (url.StartsWith("http://github.com/"))
            url = url.Substring("http://github.com/".Length);
        else if (url.StartsWith("https://github.com/"))
            url = url.Substring("https://github.com/".Length);
        else if (url.StartsWith("http://gitlab.com/"))
            url = url.Substring("http://gitlab.com/".Length);
        else if (url.StartsWith("https://gitlab.com/"))
            url = url.Substring("https://gitlab.com/".Length);
        if (url.EndsWith("/"))
            url = url.Substring(0, url.Length - 1);
        return url.Trim();
    }
    public void AddRepo(GitProvider provider, string repo, long channelId) 
        => dict.Add(new Repo(provider, repo, channelId));
    public void RemoveRepo(GitProvider provider, string repo)
        => dict.Remove(dict.Find(x => x.Provider == provider && x.Repository == repo));
}
