﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Nickbot.Module.ModuleAttributes(
    "SampleModule", // Name
    "0.1", // Version
    "Nick \"Tiaqo\" Strohm", // Author
    "" // Description
    )]
[Nickbot.Module.ModuleHandlerAttributes(
    "" // onReceivedMessageHandler(DiscordSharp.Objects.DiscordMessage message, string prefix)
    )]
public class Main : Nickbot.Module.Module
{

    public override void onLoad()
    {
        //SendLogMessage($"This is an example");
    }

    public override void onUnload()
    {

    }

    public void onMessage(DSharpPlus.Objects.DiscordMessage message, string prefix)
    {
        //SendLogMessage($"Got message: {message.Content} from {message.Author.Username} in {message.Channel().Name} on {message.Channel().parent.name} by {message.Channel().parent.owner.Username}");
    }
}
