﻿using DSharpPlus.Commands;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nickbot.CommandProvider;
using Nickbot.Module;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Timers;

[ModuleAttributes(
    "Youtube", "0.1", "Nick \"Tiaqo\" Strohm", ""
    )]
[ModuleHandlerAttributes(
    ""
    )]
public class Main : Module
{
    public class Notification
    {
        public class YouTube
        {
            [JsonProperty("channelid")]
            public string channelId { get; internal set; }
            [JsonProperty("lastChecked")]
            public DateTime lastChecked { get; internal set; }
        }

        [JsonProperty("serverId")]
        public string serverId { get; internal set; }
        [JsonProperty("channels")]
        public List<YouTube> channels { get; internal set; }
        [JsonProperty("channelId")]
        public string channelId { get; internal set; }
    }

    public List<Notification> dict = new List<Notification>();
    public Timer timer1;

    public override void onLoad()
    {
        /*if (File.Exists("youtubechannels.json"))
        {
            dict = JsonConvert.DeserializeObject<List<Notification>>(File.ReadAllText("youtubechannels.json"));
        }*/

        JArray arr = JArray.Parse(loadConfig());
        foreach(JObject JNotification in arr)
        {
            dict.Add(new Notification()
            {
                channelId = JNotification["channelId"].ToString(),
                channels = new List<Notification.YouTube>(),
                serverId = JNotification["serverId"].ToString()
            });
            foreach(JObject JChannel in JNotification["channels"])
            {
                dict[dict.Count - 1].channels.Add(new Notification.YouTube()
                {
                    channelId = JChannel["channelId"].ToString(),
                    lastChecked = DateTime.Parse(JChannel["lastChecked"].ToString())
                });
            }
        }

        timer1 = new Timer();
        // 1000 Milliseconds * 60 Seconds * 10 Minutes = 10 Minutes
        timer1.Interval = 1000 * 60 * 10;
        timer1.Elapsed += Timer1_Elapsed;

        AddCommand(new CommandStub("youtube", "Verwaltet die Channel für die YouTube Notifications", "", PermissionType.Mod, 2, e => {
            if (dict.Find(x => x.serverId == e.Channel.Parent.ID) == null)
                dict.Add(new Notification() { serverId = e.Channel.Parent.ID, channels = new List<Notification.YouTube>() });


            switch(e.Args[0])
            {
                case "add":
                    JObject JResponse = getJsonResponse($"https://www.googleapis.com/youtube/v3/search?part=snippet,id&channelId={e.Args[1]}&order=date&maxResults=20&key=AIzaSyDhkaN26o334hkMfOoy8GocM8-BqedZ8v8");
                    if (JResponse["error"] != null)
                    {
                        e.Channel.SendMessage($"<@{e.Author.ID}> Es ist ein Fehler aufgetreten! {JResponse["message"].ToString()} [{JResponse["code"].ToString()}]");
                    }
                    else
                    {
                        Notification.YouTube yt = new Notification.YouTube();
                        yt.channelId = e.Args[1];
                        yt.lastChecked = DateTime.Now;

                        dict.Find(x => x.serverId == e.Channel.Parent.ID).channels.Add(yt);

                        e.Channel.SendMessage($"<@{e.Author.ID}> Der Kanal {e.Args[1]} wurde zu den Notifications hinzugefügt!");
                        File.WriteAllText("youtubechannels.json", JsonConvert.SerializeObject(dict));
                    }
                    break;
                case "remove":

                    break;
                case "list":
                    string tOutput = $"<@{e.Author.ID}>{System.Environment.NewLine}```{System.Environment.NewLine}";
                    foreach (Notification notification in dict)
                    {
                        foreach (Notification.YouTube youtube in notification.channels)
                        {
                            tOutput = $"{tOutput}{notification.serverId} {youtube.channelId} {youtube.lastChecked}{System.Environment.NewLine}";
                        }
                    }
                    tOutput = $"{tOutput}```";
                    e.Channel.SendMessage(tOutput);
                    break;
            } 
        }));
        
        timer1.Start();
    }

    public override void onUnload()
    {
        RemoveCommand("youtube");

        timer1.Stop();

        File.WriteAllText("youtubechannels.json", JsonConvert.SerializeObject(dict));
    }

    private void Timer1_Elapsed(object sender, ElapsedEventArgs e)
    {
        //throw new NotImplementedException();
    }

    public JObject getJsonResponse(string url)
    {
        try
        {
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.Method = "GET";

            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();

            using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
            {
                return JObject.Parse(sr.ReadToEnd());
            }
        }
        catch (WebException ex)
        {
            return JObject.Parse(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
        }
    }
}
