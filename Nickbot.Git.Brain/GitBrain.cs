﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Nickbot.Git.Brain
{
    public class GitBrain
    {
        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        public event EventHandler<RepositoryUpdatedEventArgs> RepositoryUpdated;

        private CancellationTokenSource tokenSource;
        private CancellationToken token;
        private List<Repo> dict = new List<Repo>();
        private string githubToken;
        private string gitlabToken;
        private Web request = new Web();

        public GitBrain(List<Repo> repos, string githubToken, string gitlabToken)
        {
            dict = repos;
            this.githubToken = githubToken;
            this.gitlabToken = gitlabToken;

            Console.WriteLine($"Received {dict.Count} repositories");
            Console.WriteLine($"    Received {dict.Where(x => x.Provider == GitProvider.Github).Count()} repositories with github provider");
            Console.WriteLine($"    Received {dict.Where(x => x.Provider == GitProvider.Gitlab).Count()} repositories with gitlab provider");
            Console.WriteLine($"githubToken == {this.githubToken}");
            Console.WriteLine($"gitlabToken == {this.gitlabToken}");

            tokenSource = new CancellationTokenSource();
            token = tokenSource.Token;
        }

        public void Start()
        {
            Console.WriteLine($"Starting");
            Task.Run(Run);
        }

        public void Stop()
        {
            Console.WriteLine($"Stopping");
            tokenSource.Cancel();
        }

        public void UpdateRepos(List<Repo> repos) { dict = repos; }

        private async Task Run()
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                while(!token.IsCancellationRequested)
                {
                    foreach (var repo in dict)
                    {
                        try
                        {
                            if (repo.Provider == GitProvider.Github)
                            {
                                bool dateChanged = false;
                                var since = repo.LastUpdate.ToString("yyyy-MM-ddTHH:mm:ssZ");
                                DateTimeOffset newDate = repo.LastUpdate;
                                string repoAuthor = repo.Repository.Split('/')[0];
                                System.Net.WebHeaderCollection headers = new System.Net.WebHeaderCollection();
                                headers.Add(System.Net.HttpRequestHeader.Authorization, $"token {githubToken}");
                                string response;
                                JToken json;

                                foreach (var branch in repo.Branches)
                                {
                                    try
                                    {
                                        request.GET($"https://api.github.com/repos/{repo.Repository}/commits?sha={branch}&since={since}", headers);
                                    }
                                    catch
                                    {
                                        Console.WriteLine($"Unable to access {repo.Repository}'s {branch} branch");
                                        continue;
                                    }
                                    response = request.response;
                                    json = JsonConvert.DeserializeObject(response) as JToken;

                                    if (json.Children().Any())
                                    {
                                        builder.Clear();
                                        builder.Append($"**{repo.Repository}**");
                                        if (branch != "master")
                                            builder.Append($" (**{branch}**)");

                                        foreach(var commit in json.Children().Reverse())
                                        {
                                            var sha = commit.Value<string>("sha")?.Substring(0, 7);
                                            var msg = commit["commit"].Value<string>("message");
                                            var date = new DateTimeOffset(commit["commit"]["committer"].Value<DateTime>("date").AddSeconds(1.0), TimeSpan.Zero);
                                            var author = commit["commit"]["committer"].Value<string>("name");
                                            //var url = commit.Value<string>("html_url");

                                            Console.WriteLine($"Github {repo.Repository} {branch} #{sha}");
                                            string prefix = $"\n``{sha}`` ";
                                            builder.Append($"{prefix}{$@"{(msg.Split('\n')[0])}"} [{author}]");
                                            //builder.Append($"{prefix}{url}");
                                            if (date > newDate)
                                            {
                                                newDate = date;
                                                dateChanged = true;
                                            }
                                        }
                                        MessageReceived?.Invoke(this, new MessageReceivedEventArgs() { Message = builder.ToString(), Repository = repo });
                                    }
                                    await Task.Delay(1000, token);
                                }
                                try
                                {
                                    request.GET($"https://api.github.com/repos/{repo.Repository}/issues?state=all&sort=updated&since={since}");
                                }
                                catch
                                {
                                    Console.WriteLine($"Unable to access {repo.Repository}'s issues");
                                    continue;
                                }
                                response = request.response;
                                json = JsonConvert.DeserializeObject(response) as JToken;
                                foreach (var issue in json.Children().Reverse())
                                {
                                    var author = issue["user"].Value<string>("login");
                                    var id = issue.Value<string>("number");
                                    var url = issue.Value<string>("html_url");
                                    var createdAt = issue.Value<DateTime>("created_at");
                                    var updatedAt = issue.Value<DateTime>("updated_at");
                                    var closedAt = issue.Value<DateTime?>("closed_at");
                                    var title = issue.Value<string>("title");

                                    string text;
                                    bool skip = false;
                                    string type = (issue.Value<JToken>("pull_request") != null) ? "Pull Request" : "Issue";

                                    if (author.Equals(repoAuthor, StringComparison.OrdinalIgnoreCase))
                                        skip = true;
                                    if (updatedAt == closedAt)
                                        text = $"Closed {type} #{id}";
                                    else if (createdAt == updatedAt)
                                        text = $"New {type} #{id}";
                                    else
                                    {
                                        skip = true;
                                        text = $"Updated {type} #{id}";
                                    }
                                    Console.WriteLine($"{repo.Repository} {text}");
                                    if (!string.IsNullOrEmpty(title))
                                        text += '\n' + title;

                                    if (!skip)
                                    {
                                        MessageReceived?.Invoke(this, new MessageReceivedEventArgs() { Message = $"**{repo.Repository}** {text}\n{$@"{url}"}", Repository = repo });
                                    }

                                    var date = new DateTimeOffset(updatedAt.AddSeconds(1.0), TimeSpan.Zero);
                                    if (date > newDate)
                                    {
                                        newDate = date;
                                        dateChanged = true;
                                    }
                                }

                                if (dateChanged)
                                {
                                    repo.LastUpdate = newDate;
                                    RepositoryUpdated?.Invoke(this, new RepositoryUpdatedEventArgs() { Repository = repo });
                                }
                            }
                            else if (repo.Provider == GitProvider.Gitlab)
                            {
                                bool dateChanged = false;
                                var since = repo.LastUpdate.ToString("yyyy-MM-ddTHH:mm:ssZ");
                                DateTimeOffset newDate = repo.LastUpdate;
                                string repoAuthor = repo.Repository.Split('/')[0];
                                System.Net.WebHeaderCollection header = new System.Net.WebHeaderCollection();
                                header.Add("Private-Token", gitlabToken);
                                string response;
                                JToken json;
                                
                                foreach(var branch in repo.Branches)
                                {
                                    try
                                    {
                                        request.GET($"https://gitlab.com/api/v3/projects/{repo.Repository.Replace("/", "%2F")}/repository/commits?ref_name={branch}&since={since}", header);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine($"Unable to access {repo.Repository}'s {branch} branch");
                                        Console.WriteLine($"{ex.Message}");
                                        continue;
                                    }
                                    response = request.response;
                                    json = JsonConvert.DeserializeObject(response) as JToken;

                                    if (json.Children().Any())
                                    {
                                        builder.Clear();
                                        builder.Append($"**{repo.Repository}**");
                                        if (branch != "master")
                                            builder.Append($" (**{branch}**)");

                                        foreach (var commit in json.Children().Reverse())
                                        {
                                            var sha = commit.Value<string>("id")?.Substring(0,7);
                                            var msg = commit.Value<string>("message");
                                            var date = new DateTimeOffset(commit.Value<DateTime>("created_at").AddSeconds(0.1));
                                            var author = commit.Value<string>("author_name");
                                            //var url = commit.Value<string>("html_url");

                                            Console.WriteLine($"Gitlab {repo.Repository} {branch} #{sha}");
                                            string prefix = $"\n``{sha}`` ";
                                            builder.Append($"{prefix}{$@"{(msg.Split('\n')[0])}"} [{author}]");
                                            //builder.Append($"{prefix}{url}");
                                            if (date > newDate)
                                            {
                                                newDate = date;
                                                dateChanged = true;
                                            }
                                        }
                                        MessageReceived?.Invoke(this, new MessageReceivedEventArgs() { Message = builder.ToString(), Repository = repo });
                                    }
                                    await Task.Delay(1000, token);
                                }

                                try
                                {
                                    request.GET($"https://gitlab.com/api/v3/projects/{repo.Repository.Replace("/", "%2F")}/issues?since={since}&order_by=updated_at", header);
                                }
                                catch
                                {
                                    Console.WriteLine($"Unable to access {repo.Repository}'s issues");
                                    continue;
                                }
                                response = request.response;
                                json = JsonConvert.DeserializeObject(response) as JToken;

                                foreach (var issue in json.Children().Reverse())
                                {
                                    var author = issue["author"].Value<string>("name");
                                    var id = issue.Value<string>("iid");
                                    var url = issue.Value<string>("web_url");
                                    var createdAt = issue.Value<DateTime>("created_at");
                                    var updatedAt = issue.Value<DateTime>("updated_at");
                                    var closedAt = issue.Value<DateTime?>("closed_at");
                                    var title = issue.Value<string>("title");

                                    string text;
                                    bool skip = false;
                                    string type = (issue.Value<JToken>("pull_request") != null) ? "Pull Request" : "Issue";

                                    if (author.Equals(repoAuthor, StringComparison.OrdinalIgnoreCase))
                                        skip = true;
                                    if (updatedAt == closedAt)
                                        text = $"Closed {type} #{id}";
                                    else if (createdAt == updatedAt)
                                        text = $"New {type} #{id}";
                                    else
                                    {
                                        skip = true;
                                        text = $"Updated {type} #{id}";
                                    }
                                    Console.WriteLine($"{repo.Repository} {text}");
                                    if (!string.IsNullOrEmpty(title))
                                        text += '\n' + title;

                                    if (!skip)
                                    {
                                        MessageReceived?.Invoke(this, new MessageReceivedEventArgs() { Message = $"**{repo.Repository}** {text}\n{$@"{url}"}", Repository = repo });
                                    }

                                    var date = new DateTimeOffset(updatedAt.AddSeconds(1.0), TimeSpan.Zero);
                                    if (date > newDate)
                                    {
                                        newDate = date;
                                        dateChanged = true;
                                    }
                                }

                                if (dateChanged)
                                {
                                    repo.LastUpdate = newDate;
                                    RepositoryUpdated?.Invoke(this, new RepositoryUpdatedEventArgs() { Repository = repo });
                                }
                            }
                        }
                        catch (Exception ex) when (!(ex is TaskCanceledException))
                        {
                            Console.WriteLine($"GitBrain: {ex}");
                            await Task.Delay(5000);
                            continue;
                        }

                        await Task.Delay(1000, token);
                    }
                    await Task.Delay(60000, token);
                }
            }
            catch(TaskCanceledException) {  }
        }
    }

    public enum GitProvider
    {
        Gitlab = 0,
        Github = 1
    }

    public class Repo
    {
        public GitProvider Provider { get; set; }
        public string Repository { get; set; }
        public string[] Branches { get; set; }
        public long ChannelID { get; set; }
        public DateTimeOffset LastUpdate { get; set; }

        public Repo(GitProvider provider, string repository, long channelId)
        {
            Provider = provider;
            Repository = repository;
            ChannelID = channelId;
            LastUpdate = DateTimeOffset.UtcNow;
            Branches = new string[] { "master" };
        }

        public bool AddBranch(string branch)
        {
            var oldBranches = Branches;
            if (oldBranches.Contains(branch))
                return false;

            var newBranches = new string[oldBranches.Length + 1];
            Array.Copy(oldBranches, newBranches, oldBranches.Length);
            newBranches[oldBranches.Length] = branch;

            Branches = newBranches;
            return true;
        }

        public bool RemoveBranch(string branch)
        {
            var oldBranches = Branches;
            if (!oldBranches.Contains(branch))
                return false;

            Branches = oldBranches.Where(x => x != branch).ToArray();
            return true;
        }
    }

    public class MessageReceivedEventArgs : EventArgs
    {
        public Repo Repository { get; set; }
        public string Message { get; set; }
    }

    public class RepositoryUpdatedEventArgs : EventArgs
    {
        public Repo Repository { get; set; }
    }
}
