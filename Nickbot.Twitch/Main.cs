﻿#define debug

using DSharpPlus.Commands;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nickbot;
using Nickbot.CommandProvider;
using Nickbot.Module;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Timers;

[ModuleAttributes(
    "Twitch",
    "0.1",
    "Nick \"Tiaqo\" Strohm",
    "")]
[ModuleHandlerAttributes(
    ""
    )]
public class Main : Module
{
    public class Notification
    {
        public class Twitch
        {
            [JsonProperty("channelName")]
            public string channelName { get; internal set; }
            [JsonProperty("lastChecked")]
            public DateTime lastChecked { get; internal set; }
            [JsonProperty("wasOnline")]
            public bool wasOnline { get; internal set; }
            [JsonProperty("lastGame")]
            public string lastGame { get; internal set; }
            [JsonProperty("lastTitle")]
            public string lastTitle { get; internal set; }

            /*public Twitch(JObject JTwitch)
            {
                this.channelName = JTwitch["channelName"].ToString();
                this.lastChecked = DateTime.Parse(JTwitch["lastChecked"].ToString());
                this.wasOnline = bool.Parse(JTwitch["wasOnline"].ToString());
            }*/
        }

        [JsonProperty("serverId")]
        public string serverId { get; internal set; }
        [JsonProperty("channels")]
        public List<Twitch> channels { get; internal set; }
        [JsonProperty("channelId")]
        public string channelId { get; internal set; }

        /*public Notification(JObject JNotification)
        {
            this.serverId = JNotification["serverId"].ToString();
            this.channels = new List<Twitch>();
            foreach (JObject JTwitch in JNotification["channels"])
                this.channels.Add(new Twitch(JTwitch));
            this.channelId = JNotification["channelId"].ToString();
        }*/
    }

    public List<Notification> dict = new List<Notification>();
    public Timer timer1;

    private string client_id = "";

    public override void onLoad()
    {
        JArray arr = JArray.Parse(loadConfig());
        //JArray arr = JArray.Parse(File.ReadAllText("modules/Nickbot.Twitch.json"));
        foreach (JObject JNotification in arr)
        {
            dict.Add(new Notification()
            {
                channelId = JNotification["channelId"].ToString(),
                channels = new List<Notification.Twitch>(),
                serverId = JNotification["serverId"].ToString()
            });
            foreach(JObject JChannel in JNotification["channels"])
            {
                dict[dict.Count - 1].channels.Add(new Notification.Twitch()
                {
                    channelName = JChannel["channelName"].ToString(),
                    lastChecked = DateTime.Parse(JChannel["lastChecked"].ToString()),
                    wasOnline = bool.Parse(JChannel["wasOnline"].ToString()),
                    lastGame = JChannel["lastGame"].ToString(),
                    lastTitle = JChannel["lastTitle"].ToString()
                });
            }
        }

        if (!File.Exists("Nickbot.Twitch.Credentials.txt")) return;

        client_id = File.ReadAllText("Nickbot.Twitch.Credentials.txt");

        timer1 = new Timer();
        // 1000 Milliseconds * 60 Seconds * 10 Minutes = 10 Minutes
#if debug
        timer1.Interval = 1000/* * 60 * 10*/;
#else
        timer1.Interval = 1000 * 60/* * 10*/;
#endif
        timer1.Elapsed += Timer1_Elapsed;
        
        AddCommand(new CommandStub("twitch", "Manages the channel for Twitch notifications.", "", PermissionType.Mod, 2, e => {
            if (dict.Find(x => x.serverId == e.Channel.Parent.ID) == null)
                dict.Add(new Notification() { serverId = e.Channel.Parent.ID, channels = new List<Notification.Twitch>(), channelId = e.Channel.ID });
            if (dict.Find(x => x.serverId == e.Channel.Parent.ID) == null)
            {
                dict.Add(new Notification()
                {
                    channelId = e.Channel.ID,
                    channels = new List<Notification.Twitch>(),
                    serverId = e.Channel.Parent.ID
                });
            }

            switch (e.Args[0])
            {
                case "add":
                    JObject JResponse = getJsonResponse($"https://api.twitch.tv/kraken/streams/{e.Args[1]}?client_id={client_id}");
                    if (JResponse["error"] != null)
                    {
                        e.Channel.SendMessage($"<@{e.Author.ID}> An error occured! {JResponse["message"].ToString()} [{JResponse["status"].ToString()}]");
                    }
                    else
                    {
                        Notification.Twitch twitch = new Notification.Twitch();
                        twitch.channelName = e.Args[1];
                        twitch.lastChecked = DateTime.Now;
                        twitch.wasOnline = false;

                        dict.Find(x => x.serverId == e.Channel.Parent.ID).channels.Add(twitch);

                        e.Channel.SendMessage($"<@{e.Author.ID}> The channel ``{e.Args[1]}`` was successfully added to the notificationlist!");

                        saveConfig(JsonConvert.SerializeObject(dict));

                        Notification.Twitch channel = dict.Find(x => x.serverId == e.Channel.Parent.ID).channels.Find(y => y.channelName == twitch.channelName);

                        if (isStreamOnline(channel))
                        {
                            client.SendMessageToChannel($"``{channel.channelName}`` is now live on twitch! https://www.twitch.tv/{channel.channelName}", client.GetServersList().Find(x => x.ID == e.Channel.Parent.ID).Channels.Find(z => z.ID == e.Channel.ID), false);
                        }
                    }
                    break;
                case "remove":
                    if (dict.Find(x => x.serverId == e.Channel.Parent.ID).channels.Find(y => y.channelName == e.Args[1]) != null)
                    {
                        dict.Find(x => x.serverId == e.Channel.Parent.ID).channels.Remove(dict.Find(x => x.serverId == e.Channel.Parent.ID).channels.Find(y => y.channelName == e.Args[1]));
                        e.Channel.SendMessage($"<@{e.Author.ID}> The channel ``{e.Args[1]}`` was successfully removed from the notificationlist!");
                        saveConfig(JsonConvert.SerializeObject(dict));
                    }
                    else
                        e.Channel.SendMessage($"<@{e.Author.ID}> The channel ``{e.Args[1]}`` was not found in the notificationlist! Type ``twitch list``");
                    break;
                case "list":
                    string tOutput = "";
                    tOutput = $"<@{e.Author.ID}>{System.Environment.NewLine}```{System.Environment.NewLine}{{0,-24}} | {{1,-19}} | {{2,-7}} | {{3,-20}} | {{4}}{System.Environment.NewLine}";
                    tOutput = string.Format(tOutput, "Channel:", "Last checked:", "Online:", "Game:", "Title:");
                    foreach (Notification.Twitch twitch in dict.Find(x => x.serverId == e.Channel.Parent.ID).channels)
                    {
                        string output = string.Format("{0,-24} | {1,-19} | {2,-7} | {3,-20} | {4}", twitch.channelName, twitch.lastChecked.ToString(), twitch.wasOnline, twitch.lastGame, twitch.lastTitle);
                        tOutput = $"{tOutput}{output}{System.Environment.NewLine}";
                    }
                    tOutput = $"{tOutput}```";
                    e.Channel.SendMessage(tOutput);
                    break;
            }
        }));

        timer1.Start();
    }

    public override void onUnload()
    {
        RemoveCommand("twitch");

        timer1.Stop();

        saveConfig(JsonConvert.SerializeObject(dict));
    }

    private void Timer1_Elapsed(object sender, ElapsedEventArgs e)
    {
        foreach (Notification notification in dict)
        {
            if (notification.channels != null && notification.channels.Count > 0)
                foreach (Notification.Twitch channel in notification.channels)
                {
                    if (isStreamOnline(channel))
                    {
                        if (channel.wasOnline == false)
                        {
                            channel.wasOnline = true;
                            client.SendMessageToChannel($"``{channel.channelName}`` is now live on twitch! https://www.twitch.tv/{channel.channelName}", client.GetServersList().Find(x => x.ID == notification.serverId).Channels.Find(z => z.ID == notification.channelId), false);
                        }
                    }
                    else
                    {
                        channel.wasOnline = false;
                    }
                }
        }
        saveConfig(JsonConvert.SerializeObject(dict));
    }

    public bool isStreamOnline(Notification.Twitch channel)
    {
        JObject JResponse = getJsonResponse($"https://api.twitch.tv/kraken/streams/{channel.channelName}");
        if (JResponse["stream"] != null && JResponse["stream"].HasValues)
        {
            channel.lastChecked = DateTime.Now;
            channel.lastGame = JResponse["stream"]["channel"]["game"].ToString();
            channel.lastTitle = JResponse["stream"]["channel"]["status"].ToString();
            return true;
        }
        else
        {
            channel.lastChecked = DateTime.Now;
            return false;
        }
    }

    public JObject getJsonResponse(string url)
    {
        try
        {
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(url);
            httpRequest.Method = "GET";
            httpRequest.Accept = "application/vnd.twitchtv.v3+json";

            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();

            using (StreamReader sr = new StreamReader(httpResponse.GetResponseStream()))
            {
                return JObject.Parse(sr.ReadToEnd());
            }
        }
        catch (WebException ex)
        {
            return JObject.Parse(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
        }
    }
}